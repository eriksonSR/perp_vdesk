import gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from models.models import *

class WinListaEstudios:
	def __init__(self):
		self.handlers = {
			"busca_estudio": self.BuscaEstudio
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/estudios/win_lista_estudios.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_studios = StudiosModel()
		self.list_estudios = Gtk.ListStore(int, str, str)
		self.tree_estudios = self.builder.get_object('treeview_estudios')
		self.ExibeLista()
		self.win_lista_estudio = self.builder.get_object("win_lista_estudios")
		self.win_lista_estudio.connect("destroy", Gtk.main_quit)
		self.win_lista_estudio.show_all()

		Gtk.main()

	def ExibeLista(self):
		estudios = self.m_studios.Listar()
		for estudio in estudios:
			self.list_estudios.append([estudio['id'], estudio['studio'], 'document-edit'])

		self.tree_estudios.set_model(self.list_estudios)
		
		renderer_id = Gtk.CellRendererText()
		column_id = Gtk.TreeViewColumn("ID", renderer_id, text=0)
		self.tree_estudios.append_column(column_id)
		
		renderer_estudio = Gtk.CellRendererText()
		column_autor = Gtk.TreeViewColumn("Estúdio", renderer_estudio, text=1)
		self.tree_estudios.append_column(column_autor)

		renderer_editar = Gtk.CellRendererPixbuf()
		renderer_editar.set_property('xalign', 0.0)
		column_editar = Gtk.TreeViewColumn("Editar", renderer_editar, icon_name=2)
		self.tree_estudios.append_column(column_editar)

		self.tree_estudios.set_activate_on_single_click(True)
		self.tree_estudios.connect("row_activated", self.EditarEstudio)

	def EditarEstudio(self, treeview, path, column):
		if column.get_title() == 'Editar':
			model, treeiter = treeview.get_selection().get_selected()
			id_estudio = model[treeiter][0]
			win_edita_estudio = WinEditaEstudio(id_estudio)

	def BuscaEstudio(self, widget):
		entry = self.builder.get_object('entry_estudio')
		estudio = entry.get_text()
		qtd_chars = len(estudio)
		
		if qtd_chars >= 3:
			self.list_estudios.clear()
			estudios = self.m_studios.GetStudiosByNome(estudio)

			for estudio in estudios:
				self.list_estudios.append([estudio['id'], estudio['studio'], 'document-edit'])
			
			self.tree_estudios.set_model(self.list_estudios)
		elif qtd_chars == 0:
			self.list_estudios.clear()
			estudios = self.m_studios.Listar()

			for estudio in estudios:
				self.list_estudios.append([estudio['id'], estudio['studio'], 'document-edit'])
			
			self.tree_estudios.set_model(self.list_estudios)

class WinEditaEstudio:
	def __init__(self, id_estudio):
		self.id_estudio = id_estudio
		self.handlers = {
			"salvar_estudio": self.SalvaEstudio
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/estudios/win_edita_estudio.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_edita_estudio = self.builder.get_object("win_edita_estudio")
		self.win_edita_estudio.connect("destroy", Gtk.main_quit)
		self.win_edita_estudio.show_all()
		self.GetEstudio()

		Gtk.main()

	def SalvaEstudio(self, widget):
		entry = self.builder.get_object('entry_estudio')
		estudio_nome = entry.get_text()
		estudio = {'estudio':estudio_nome, 'id':self.id_estudio}
		m_estudio = StudiosModel()
		if not m_estudio.TestaSeExisteStudio(estudio_nome):
			m_estudio.Atualizar(estudio)
			self.Info('sucesso', 'Estúdio atualizado com sucesso!')
		else:
			self.Info('erro', 'Estúdio já cadastrado!')

	def GetEstudio(self):
		m_estudio = StudiosModel()
		estudio = m_estudio.GetStudioById(self.id_estudio)

		entry = self.builder.get_object('entry_estudio')
		entry.set_text(estudio['studio'])

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()

class WinAdicionaEstudio:
	def __init__(self):
		self.handlers = {
			"add_estudio": self.AdicionaEstudio
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/estudios/win_add_estudio.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_add_estudio = self.builder.get_object("win_add_estudio")
		self.win_add_estudio.connect("destroy", Gtk.main_quit)
		self.win_add_estudio.show_all()

		Gtk.main()

	def AdicionaEstudio(self, widget):
		entry = self.builder.get_object('entry_estudio')
		estudio = entry.get_text()
		m_estudio = StudiosModel()
		if not m_estudio.TestaSeExisteStudio(estudio):
			m_estudio.Inserir(estudio)
			entry.set_text('')
			self.Info('sucesso', 'Estúdio cadastrado com sucesso!')
		else:
			self.Info('erro', 'Estúdio já cadastrado!')

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()