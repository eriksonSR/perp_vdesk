import requests, json

class GamesDataBaseApi():
	def __init__(self, id_game):
		self.id_game = id_game
		self.dir_json = './dados_apis/gamesdb/json/'
		self.api_key = 'fb8e1fbf2d63928bcf80ef498fc1cd5bfbc20b1bc99a8cb196db9e65d1549343'
		self.url_api_game = 'https://api.thegamesdb.net/'

	def GetJson(self):
		fields = 'Games/Images?&filter[type]=boxart,fanart,screenshot&games_id={}&apikey={}'.format(self.id_game, self.api_key)

		j = requests.get('{}{}'.format(self.url_api_game, fields)).json()

		arq = r'./dados_apis/gamesdb/json/{}.json'.format(self.id_game)
		with open(arq, 'w') as arq_j:
			json.dump(j, arq_j)

	def GetBoxArtsUrls(self):
		urls_boxes = {'originais':[],'thumbs':[]}
		arq = r'./dados_apis/gamesdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)
			base_url = j['data']['base_url']['original']

			for i in j['data']['images']['{}'.format(self.id_game)]:
				if i['type'] == 'boxart' and i['side'] == 'front':
					urls_boxes['originais'].append('{}{}'.format(base_url, i['filename']))

		return urls_boxes

	def GetScreenshotsUrls(self):
		urls_screenshots = {'originais':[],'thumbs':[]}
		arq = r'./dados_apis/gamesdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)
			base_url = j['data']['base_url']['original']

			for i in j['data']['images']['{}'.format(self.id_game)]:
				if i['type'] == 'screenshot':
					urls_screenshots['originais'].append('{}{}'.format(base_url, i['filename']))

		return urls_screenshots

	def GetArtWorksUrls(self):
		urls_fanart = {'originais':[],'thumbs':[]}
		arq = r'./dados_apis/gamesdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)
			base_url = j['data']['base_url']['original']

			for i in j['data']['images']['{}'.format(self.id_game)]:
				if i['type'] == 'fanart':
					urls_fanart['originais'].append('{}{}'.format(base_url, i['filename']))

		return urls_fanart
	

class SkoobApi():
	def __init__(self, id_livro, dowload = 0):
		self.id_user = 1026257
		self.id_livro = id_livro
		self.url_api = 'https://www.skoob.com.br/v1/book/{}/user_id:{}/'.format(id_livro, self.id_user)
		self.dir_json = './dados_apis/skoob/json/'

		if dowload:
			self.GetJson()
			
	def GetJson(self):
		sk_json = requests.get(self.url_api).json()
		arq = r'{}{}.json'.format(self.dir_json, self.id_livro)
		
		with open(arq, 'w') as arq_j:
			json.dump(sk_json, arq_j)

	def GetCapasUrls(self):
		arq = r'{}{}.json'.format(self.dir_json, self.id_livro)
		url_imgs = {'capa_grande':'','capa_media':'','capa_pequena':'','capa_micro':'','capa_nano':''}
		
		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)

			url_imgs['capa_grande'] = j['response']['capa_grande']
			url_imgs['capa_media'] = j['response']['capa_media']
			url_imgs['capa_pequena'] = j['response']['capa_pequena']
			url_imgs['capa_micro'] = j['response']['capa_micro']
			url_imgs['capa_nano'] = j['response']['capa_nano']

		return url_imgs

class IgdbApi():
	def __init__(self, id_game):
		self.id_game = id_game
		self.dir_json = './dados_apis/igdb/json/'
		self.arq_json = '{}{}.json'.format(self.dir_json, self.id_game)
		self.api_key = '07c45d0dc693222ccb9e837f4e8d400d'
		self.api_headers = {'user-key':self.api_key, 'Accept':'application/json', 'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/68.0.3440.75 Chrome/68.0.3440.75 Safari/537.36'}
		self.url_api_game = 'https://api-endpoint.igdb.com/games/'
		self.url_img_original_game = 'https://images.igdb.com/igdb/image/upload/t_1080p/'
		self.url_img_thumb_game = 'https://images.igdb.com/igdb/image/upload/t_thumb/'

	def GetJson(self):
		fields = '?fields=id,name,url,summary,developers,publishers,dlcs,release_dates,websites,alternative_names,screenshots,artworks,videos,cover,websites,genres.name,game.name,developers.name,publishers.name&expand=game,genres,developers,publishers'
		j = requests.get('{}{}/{}'.format(self.url_api_game,self.id_game,fields), headers = self.api_headers).json()

		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)
		with open(arq, 'w') as arq_j:
			json.dump(j, arq_j)

	def GetBoxArtsUrls(self):
		urls_boxes = {'originais':[],'thumbs':[]}
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)
			id_img = j[0]['cover']['cloudinary_id']
			
			urls_boxes['originais'].append('{}{}.jpg'.format(self.url_img_original_game, id_img))
			urls_boxes['thumbs'].append('{}{}.jpg'.format(self.url_img_thumb_game, id_img))

		return urls_boxes

	def GetScreenshotsUrls(self):
		urls_screenshots = {'originais':[],'thumbs':[]}
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)
			if 'screenshots' in j[0].keys():
				for scr in j[0]['screenshots']:
					id_img = scr['cloudinary_id']
					urls_screenshots['originais'].append('{}{}.jpg'.format(self.url_img_original_game, id_img))
					urls_screenshots['thumbs'].append('{}{}.jpg'.format(self.url_img_thumb_game, id_img))

		return urls_screenshots

	def GetArtWorksUrls(self):
		urls_artworks = {'originais':[],'thumbs':[]}
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)

			if 'artworks' in j[0].keys():
				for art in j[0]['artworks']:
					id_img = art['cloudinary_id']
					urls_artworks['originais'].append('{}{}.jpg'.format(self.url_img_original_game, id_img))
					urls_artworks['thumbs'].append('{}{}.jpg'.format(self.url_img_thumb_game, id_img))

		return urls_artworks

	def GetVideosUrls(self):
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)

			if 'videos' in j[0].keys():
				return j[0]['videos']
			else:
				return False

	def GetDevelopers(self):
		desenvolvedoras = []
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)

			if 'developers' in j[0].keys():
				for dev in j[0]['developers']:
					desenvolvedoras.append(dev['name'])

				return desenvolvedoras
			else:
				return False

	def GetPublishers(self):
		publicadoras = []
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)

			if 'publishers' in j[0].keys():
				for pub in j[0]['publishers']:
					publicadoras.append(pub['name'])

				return publicadoras
			else:
				return False

	def GetGenres(self):
		generos = []
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)

			if 'genres' in j[0].keys():
				for gen in j[0]['genres']:
					generos.append(gen['name'])

				return generos
			else:
				return False

	def GetSummary(self):
		arq = r'./dados_apis/igdb/json/{}.json'.format(self.id_game)

		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)

			if 'summary' in j[0].keys():
				return j[0]['summary']
			else:
				return False