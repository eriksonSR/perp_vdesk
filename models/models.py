import psycopg2.extras, json
from pprint import pprint

conexao = psycopg2.connect(host = "localhost", dbname="db_perp", user="postgres", password="postgres")

class AutoresModel():
	def __init__(self):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)
		self.autor = ''

	def Inserir(self, autor):
		self.autor = autor
		
		try:
			self.cursor.execute("INSERT INTO tb_autores (autor) VALUES (%s)",(self.autor,))
			conexao.commit()

		except psycopg2.Error as e:
			print(e.pgerror)

	def TestaSeExisteAutor(self, autor):
		self.cursor.execute("SELECT id, autor FROM tb_autores WHERE autor = %s",(autor,))
		registros = self.cursor.fetchall()

		if(len(registros) == 0):
			return False
		else:
			return True

	def Listar(self):
		self.cursor.execute("SELECT id, autor FROM tb_autores ORDER BY autor")
		registros = self.cursor.fetchall()

		return registros

	def GetAutorByNome(self, autor):
		st = '%{}%'.format(autor)
		self.cursor.execute("SELECT id, autor FROM tb_autores WHERE autor ILIKE %s",(st,))
		autores = self.cursor.fetchall()

		return autores

	def GetAutorById(self, id_autor):
		self.cursor.execute("SELECT id, autor FROM tb_autores WHERE id = %s",(id_autor,))
		return self.cursor.fetchone()

	def Atualizar(self, autor):
		self.cursor.execute("UPDATE tb_autores SET autor = %s WHERE id = %s",(autor['autor'],autor['id'],))

class EditorasModel():
	def __init__(self):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)
		self.editora = ''

	def Inserir(self, editora):
		self.editora = editora
		
		if not self.TestaSeExisteEditora(self.editora):
			try:
				self.cursor.execute("INSERT INTO tb_editoras (editora) VALUES (%s)",(self.editora,))
				conexao.commit()

			except psycopg2.Error as e:
				print(e.pgerror)
		else:
			print('Editora {} já cadastrada!'.format(self.editora))

	def TestaSeExisteEditora(self, editora):
		self.cursor.execute("SELECT id, editora FROM tb_editoras WHERE editora = %s",(editora,))
		registros = self.cursor.fetchall()

		if(len(registros) == 0):
			return False
		else:
			return True

	def Listar(self):
		self.cursor.execute("SELECT id, editora FROM tb_editoras ORDER BY editora")
		registros = self.cursor.fetchall()

		return registros

	def BuscaEditoraPorNome(self, editora):
		st = '%{}%'.format(editora)
		self.cursor.execute("SELECT id, editora FROM tb_editoras WHERE editora ILIKE %s",(st,))
		editoras = self.cursor.fetchall()

		return editoras

	def GetEditoraById(self, id_editora):
		self.cursor.execute("SELECT id, editora FROM tb_editoras WHERE id = %s",(id_editora,))
		return self.cursor.fetchone()

	def Atualizar(self, editora):
		self.cursor.execute("UPDATE tb_editoras SET editora = %s WHERE id = %s",(editora['editora'],editora['id'],))

class AssuntosModel():
	def __init__(self):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)
		self.assunto = ''

	def Inserir(self, assunto):
		self.assunto = assunto
		
		try:
			self.cursor.execute("INSERT INTO tb_assuntos (assunto) VALUES (%s)",(self.assunto,))
			conexao.commit()

		except psycopg2.Error as e:
			print(e.pgerror)

	def TestaSeExisteAssunto(self, assunto):
		self.cursor.execute("SELECT id, assunto FROM tb_assuntos WHERE assunto = %s",(assunto,))
		assuntos = self.cursor.fetchall()

		if(len(assuntos) == 0):
			return False
		else:
			return True

	def GetAssuntoByNome(self, assunto):
		self.cursor.execute("SELECT id, assunto FROM tb_assuntos WHERE assunto = %s",(assunto,))
		assuntos = self.cursor.fetchall()

		return assuntos

	def Listar(self):
		self.cursor.execute("SELECT id, assunto FROM tb_assuntos ORDER BY assunto")
		registros = self.cursor.fetchall()

		return registros

	def GetAssuntoById(self, id_assunto):
		self.cursor.execute("SELECT id, assunto FROM tb_assuntos WHERE id = %s",(id_assunto,))
		return self.cursor.fetchone()

	def Atualizar(self, assunto):
		self.cursor.execute("UPDATE tb_assuntos SET assunto = %s WHERE id = %s",(assunto['assunto'],assunto['id'],))


class LeiturasModel():
	def __init__(self, id = False):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)

		self.id = 0
		self.ids_autores = []
		self.ids_editoras = []
		self.ids_assuntos = []
		self.id_tipo_midia = 0
		self.id_idioma = 0
		self.id_skoob = 0
		self.ano = 0
		self.digital = 0
		self.favorito = 0
		self.dt_finalizacao = '0000-00-00'
		self.nota = 0
		self.paginas = 0
		self.isbn = 0
		self.edicao = 0
		self.resenha = ''
		self.sinopse = ''
		self.titulo = ''
		self.subtitulo = ''
		self.marcacoes = {}
		self.url_capas = {}

	def Inserir(self, autores, editoras, assuntos, tipo_midia, id_skoob, digital, favorito, dt_fim, nota, pags, resenha, sinopse, titulo, subtitulo, isbn, ano, marcacoes, capas):
		if len(autores) > 1:
			self.ids_autores = autores
		else:
			self.ids_autores = autores[0]

		if len(editoras) > 1:
			self.ids_editoras = editoras
		else:
			self.ids_editoras = editoras[0]

		if len(assuntos) > 1:
			self.ids_assuntos = assuntos
		else:
			self.ids_assuntos = assuntos[0]

		self.id_skoob = id_skoob
		self.tipo_midia = tipo_midia
		self.paginas = pags
		self.dt_finalizacao = dt_fim
		self.ano = ano
		self.isbn = isbn
		self.resenha = resenha
		self.sinopse = sinopse
		self.titulo = titulo
		self.subtitulo = subtitulo
		self.marcacoes = marcacoes
		self.url_capas = capas
		self.digital = digital
		self.favorito = favorito
		self.nota = nota

		self.cursor.execute("""INSERT INTO tb_leituras (
				ids_autores, 
				ids_editoras, 
				ids_assuntos, 
				id_tipo_midia,
				titulo,
				subtitulo,
				isbn,
				ano,
				paginas,
				sinopse,
				resenha,
				nota,
				dt_finalizacao,
				id_skoob,
				favorito,
				digital,
				marcacoes,
				url_capas)
			VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
			([self.ids_autores], [self.ids_editoras], [self.ids_assuntos], self.tipo_midia, self.titulo, self.subtitulo, self.isbn,
				self.ano, self.paginas, self.sinopse, self.resenha, self.nota, self.dt_finalizacao, self.id_skoob, self.favorito, self.digital, json.dumps(self.marcacoes), json.dumps(self.url_capas))
		)
		conexao.commit()

	def Listar(self):
		pass

	def GetLeituras(self):
		pass

	def GetLivros(self):
		pass

	def GetHqs(self):
		pass

	def GetMangas(self):
		pass

	def GetLeituraById(self, id):
		self.cursor.execute("SELECT * FROM tb_leituras WHERE id = %s",(id,))

		return self.cursor.fetchone()


	def GetLeituraById(self, id_leitura):
		self.cursor.execute("""
			SELECT 
				concat(l.titulo, ' ', l.subtitulo) titulo,
				e.editora, 
				tp.tipo_midia tipo, 
				to_char(l.dt_finalizacao, 'DD/MM/YYYY') as dt_finalizacao,
			    CASE 
			        WHEN digital = 1 THEN 'Digital'
			        ELSE
						'Fisíco'
			    END AS formato,
			    l.isbn,
			    l.nota,
			    l.paginas,
			    l.ano,
			    l.id_skoob,
			    l.sinopse,
			    l.resenha
			FROM tb_leituras l
			INNER JOIN tb_editoras e on e.id = any(l.ids_editoras)
			INNER JOIN tb_tipos_midias tp on tp.id = l.id_tipo_midia
			WHERE l.id = {}
		""".format(id_leitura))

		return self.cursor.fetchone()

	def GetAutoresDaLeitura(self, id_leitura):
		self.cursor.execute("""
			SELECT
				ta.autor
			FROM tb_leituras tl
			INNER JOIN tb_autores ta ON ta.id = ANY(tl.ids_autores)
			WHERE tl.id = {}""".format(id_leitura))		

		registros = self.cursor.fetchall()
		return registros

	def GetAssuntosDaLeitura(self, id_leitura):
		self.cursor.execute("""
			SELECT
				ta.assunto
			FROM tb_leituras tl
			INNER JOIN tb_assuntos ta ON ta.id = ANY(tl.ids_assuntos)
			WHERE tl.id = {}""".format(id_leitura))		

		registros = self.cursor.fetchall()
		return registros

	def GetDadosCsv(self):
		self.cursor.execute("""
			SELECT
				l.id, 
				concat(l.titulo, ' ', l.subtitulo) titulo,
				e.editora, 
				tp.tipo_midia tipo, 
				to_char(l.dt_finalizacao, 'DD/MM/YYYY') as dt,
			    CASE 
			        WHEN digital = 1 THEN 'Digital'
			        ELSE
						'Fisíco'
			    END AS formato,
			    l.isbn,
			    l.nota,
			    l.paginas,
			    l.ano,
			    l.id_skoob,
			    l.sinopse,
			    l.resenha,
			    ta.autor,
			    tast.assunto
			FROM tb_leituras l
			INNER JOIN tb_editoras e ON e.id = ANY(l.ids_editoras)
			INNER JOIN tb_tipos_midias tp ON tp.id = l.id_tipo_midia
			INNER JOIN tb_autores ta ON ta.id = ANY(l.ids_autores)
			INNER JOIN tb_assuntos tast ON tast.id = ANY(l.ids_assuntos)
		""")
		registros = self.cursor.fetchall()

		return registros

class StudiosModel():
	def __init__(self):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)
		self.studio = ''

	def Inserir(self, studio):
		self.studio = studio
		
		if not self.TestaSeExisteStudio(self.studio):
			try:
				self.cursor.execute("INSERT INTO tb_studios (studio) VALUES (%s)",(self.studio,))
				conexao.commit()

			except psycopg2.Error as e:
				print(e.pgerror)
		else:
			print('Studio {} já cadastrado!'.format(self.studio))

	def TestaSeExisteStudio(self, studio):
		self.cursor.execute("SELECT id, studio FROM tb_studios WHERE studio = %s",(studio,))
		registros = self.cursor.fetchall()

		if(len(registros) == 0):
			return False
		else:
			return True

	def Listar(self):
		self.cursor.execute("SELECT id, studio FROM tb_studios ORDER BY studio")
		registros = self.cursor.fetchall()

		return registros

	def GetStudiosByNome(self, studio):
		self.cursor.execute("SELECT id, studio FROM tb_studios WHERE studio = '{}'".format(studio))
		studios = self.cursor.fetchall()

		return studios

	def GetStudioById(self, id_studio):
		self.cursor.execute("SELECT id, studio FROM tb_studios WHERE id = %s",(id_studio,))

		return self.cursor.fetchone()

	def Atualizar(self, studio):
		self.cursor.execute("UPDATE tb_studios SET studio = %s WHERE id = %s",(studio['estudio'],studio['id'],))

class GenerosModel():
	def __init__(self):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)
		self.genero = ''

	def Inserir(self, genero):
		self.genero = genero
		
		if not self.TestaSeExisteGenero(self.genero):
			try:
				self.cursor.execute("INSERT INTO tb_generos (genero) VALUES (%s)",(self.genero,))
				conexao.commit()

			except psycopg2.Error as e:
				print(e.pgerror)
		else:
			print('Genêro {} já cadastrado!'.format(self.genero))

	def TestaSeExisteGenero(self, genero):
		self.cursor.execute("SELECT id, genero FROM tb_generos WHERE genero = %s",(genero,))
		registros = self.cursor.fetchall()

		if(len(registros) == 0):
			return False
		else:
			return True

	def Listar(self):
		self.cursor.execute("SELECT id, genero FROM tb_generos ORDER BY genero")
		registros = self.cursor.fetchall()

		return registros

	def GetGenerosByNome(self, genero):
		self.cursor.execute("SELECT id, genero FROM tb_generos WHERE genero = '{}'".format(genero))
		generos = self.cursor.fetchall()

		if len(generos) >= 1:
			return generos
		else:
			return False

	def GetGeneroById(self, id_genero):
		self.cursor.execute("SELECT * FROM tb_generos WHERE id = %s",(id_genero,))

		return self.cursor.fetchone()

	def Atualizar(self, id_genero, genero):
		self.cursor.execute("UPDATE tb_generos SET genero = %s WHERE id = %s",(genero, id_genero,))

class GamesModel():
	def __init__(self, id = False):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)

		self.id = 0
		self.ids_desenvolvedoras = []
		self.ids_publicadoras = []
		self.ids_generos = []
		self.digital = 0
		self.dlc = 0
		self.dt_finalizacao = '0000-00-00'
		self.sinopse = ''
		self.titulo = ''
		self.videos = {}
		self.ids_apis = {}
		self.imagens = {}

	def Inserir(self, titulo, sinopse, dlc, digital, dt_finalizacao, desenvolvedoras, publicadoras, generos, videos, imagens, id_igdb, id_gdb, id_console):
		self.ids_desenvolvedoras = desenvolvedoras
		self.ids_publicadoras = publicadoras
		self.ids_generos = generos
		self.id_console = id_console
		self.id_igdb = id_igdb
		self.id_gdb = id_gdb
		self.digital = digital
		self.dlc = dlc
		self.dt_finalizacao = dt_finalizacao
		self.sinopse = sinopse
		self.titulo = titulo
		self.videos = videos
		self.imagens = imagens

		self.cursor.execute("""INSERT INTO tb_games (
				ids_desenvolvedoras, 
				ids_publicadoras, 
				ids_generos,
				id_console,
				id_igdb,
				id_gamesdb,
				digital, 
				dlc, 
				dt_finalizacao, 
				sinopse, 
				titulo, 
				videos,
				imagens) 
			VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
			([self.ids_desenvolvedoras], [self.ids_publicadoras], [self.ids_generos], self.id_console, self.id_igdb, self.id_gdb, self.digital, self.dlc, self.dt_finalizacao, 
				self.sinopse, self.titulo, json.dumps(self.videos),json.dumps(self.imagens))
		)
		conexao.commit()

	def Listar(self):
		self.cursor.execute("SELECT * FROM tb_games")
		registros = self.cursor.fetchall()

		return registros

	def GetGameById(self, id):
		self.cursor.execute("SELECT * FROM tb_games WHERE id = %s",(id,))

		return self.cursor.fetchone()

	def GetGameByIdIgdb(self, id_igdb):
		self.cursor.execute("SELECT * FROM tb_games WHERE id_igdb = %s",(id_igdb,))

		return self.cursor.fetchone()

	def GetGameByIdGamesDb(self, id_gamesdb):
		self.cursor.execute("SELECT * FROM tb_games WHERE id_gamesdb = %s",(id_gamesdb,))

		return self.cursor.fetchone()

	def GetGenerosGame(self, id_game):
		self.cursor.execute("""
			SELECT
				tgn.genero
			FROM tb_games tg
			INNER JOIN tb_generos tgn ON tgn.id = ANY(tg.ids_generos)
			WHERE tg.id = {}""".format(id_game))		

		registros = self.cursor.fetchall()
		return registros

	def GetDesenvolvedorasGame(self, id_game):
		self.cursor.execute("""
			SELECT
				ts.studio
			FROM tb_games tg
			INNER JOIN tb_studios ts ON ts.id = ANY(tg.ids_desenvolvedoras)
			WHERE tg.id = {}""".format(id_game))		

		registros = self.cursor.fetchall()
		return registros

	def GetFichaGame(self, id_game):
		self.cursor.execute("""
			SELECT
				tg.titulo, 
				to_char(tg.dt_finalizacao, 'DD/MM/YYYY') as dt,
			    CASE 
			        WHEN dlc = 1 THEN 'Sim'
			        ELSE
						'Não'
			    END AS dlc,
			    CASE 
			        WHEN digital = 1 THEN 'Sim'
			        ELSE
						'Não'
			    END AS digital,
			    tc.console,
			    tg.id_igdb,
			    tg.sinopse,
			    ts.studio
			FROM tb_games tg
			INNER JOIN tb_consoles tc ON tc.id = tg.id_console
			INNER JOIN tb_studios ts ON ts.id = ANY(tg.ids_publicadoras)
			WHERE tg.id = {}""".format(id_game))		

		return self.cursor.fetchone()

	def GetDadosCsv(self):
		self.cursor.execute("""
			SELECT
				tg.id,
				tg.titulo, 
				to_char(tg.dt_finalizacao, 'DD/MM/YYYY') as dt,
			    CASE 
			        WHEN dlc = 1 THEN 'Sim'
			        ELSE
						'Não'
			    END AS dlc,
			    CASE 
			        WHEN digital = 1 THEN 'Sim'
			        ELSE
						'Não'
			    END AS digital,
			    tc.console,
			    tg.id_igdb,
			    tg.id_gamesdb,
			    tp.studio AS publicadora,
			    td.studio AS desenvolvedora,
			    tgn.genero
			FROM tb_games tg
			INNER JOIN tb_consoles tc ON tc.id = tg.id_console
			INNER JOIN tb_studios tp ON tp.id = ANY(tg.ids_publicadoras)
			INNER JOIN tb_studios td ON td.id = ANY(tg.ids_desenvolvedoras)
			INNER JOIN tb_generos tgn ON tgn.id = ANY(tg.ids_generos)
			ORDER BY titulo
			""")
		registros = self.cursor.fetchall()

		return registros

class PlataformasModel():
	def __init__(self):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)

	def Listar(self):
		self.cursor.execute("SELECT id, plataforma FROM tb_plataformas ORDER BY plataforma")
		registros = self.cursor.fetchall()

		return registros

class ConsolesModel():
	def __init__(self):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)

	def Listar(self):
		self.cursor.execute("SELECT c.id, console, plataforma FROM tb_consoles c INNER JOIN tb_plataformas p ON p.id = c.id_plataforma ORDER BY plataforma, console")
		registros = self.cursor.fetchall()

		return registros

	def GetConsolesByNome(self, console):
		st = '%{}%'.format(console)
		self.cursor.execute("SELECT c.id, p.id id_plataforma, console, plataforma FROM tb_consoles c INNER JOIN tb_plataformas p ON p.id = c.id_plataforma WHERE console ILIKE %s",(st,))
		consoles = self.cursor.fetchall()

		return consoles

	def GetConsoleById(self, id):
		self.cursor.execute("SELECT c.id, p.id id_plataforma, console, plataforma FROM tb_consoles c INNER JOIN tb_plataformas p ON p.id = c.id_plataforma WHERE c.id = %s",(id,))

		return self.cursor.fetchone()

	def Inserir(self, id_plataforma, console):
		try:
			self.cursor.execute("INSERT INTO tb_consoles (id_plataforma, console) VALUES (%s, %s)",(id_plataforma, console,))
			conexao.commit()
		except psycopg2.Error as e:
			print(e.pgerror)

	def TestaSeExisteConsole(self, console, id_plataforma):
		self.cursor.execute("SELECT id, console FROM tb_consoles WHERE console = %s AND id_plataforma = %s",(console, id_plataforma))
		registros = self.cursor.fetchall()

		if(len(registros) == 0):
			return False
		else:
			return True

	def Atualizar(self, id_console, id_plataforma, console):
		self.cursor.execute("UPDATE tb_consoles SET id_plataforma = %s, console = %s WHERE id = %s",(id_plataforma, console, id_console,))

class GamesRelatoriosModel():
	def __init__(self, id = False):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)
			
	def ConsolidadoPorPlataforma(self, id_plataforma = None):
		ft_plataforma = ""

		if id_plataforma:
			ft_plataforma = "AND p.id = {}".format(id_plataforma)

		self.cursor.execute("""
			SELECT 
				p.plataforma, count(c.id) total
			FROM tb_consoles c
			INNER JOIN tb_games g ON g.id_console = c.id
			INNER JOIN tb_plataformas p ON p.id = c.id_plataforma
			WHERE 0 = 0
			{}
			GROUP BY p.plataforma
			ORDER BY total DESC
		""".format(ft_plataforma))

		registros = self.cursor.fetchall()
		return registros
			
	def GamesPorPlataforma(self, id_plataforma = None):
		ft_plataforma = ""

		if id_plataforma:
			ft_plataforma = "AND p.id = {}".format(id_plataforma)

		self.cursor.execute("""
			SELECT 
				p.plataforma, c.console, g.titulo
			FROM tb_consoles c
			INNER JOIN tb_games g ON g.id_console = c.id
			INNER JOIN tb_plataformas p ON p.id = c.id_plataforma
			{}
			ORDER BY p.plataforma, c.console, g.titulo
		""".format(ft_plataforma))

		registros = self.cursor.fetchall()
		return registros
			
	def ConsolidadoPorPlataformaAoAno(self, ids_plataformas = None, ano = None):
		ft_plataforma = ""
		ft_ano = ""

		if ids_plataformas:
			ft_plataforma = " AND p.id IN {}".format(ids_plataformas).replace('[','(').replace(']',')')

		if ano:
			ft_ano = " AND to_char(g.dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				p.plataforma, to_char(g.dt_finalizacao, 'YYYY') ano, count(p.id) total
			FROM tb_plataformas p
			INNER JOIN tb_consoles c ON c.id_plataforma = p.id
			INNER JOIN tb_games g ON g.id_console = c.id
			WHERE 0 = 0
			{}
			{}
			GROUP BY p.plataforma, ano
			ORDER BY ano, plataforma ASC
		""".format(ft_plataforma, ft_ano))

		registros = self.cursor.fetchall()
		return registros
			
	def GamesPorPlataformaAoAno(self, id_plataforma = None, ano = None):
		ft_plataforma = ""
		ft_ano = ""

		if id_plataforma:
			ft_plataforma = " AND p.id = {}".format(id_plataforma)

		if ano:
			ft_ano = " AND to_char(g.dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				p.plataforma, to_char(g.dt_finalizacao, 'YYYY') ano, g.titulo
			FROM tb_plataformas p
			INNER JOIN tb_consoles c ON c.id_plataforma = p.id
			INNER JOIN tb_games g ON g.id_console = c.id
			WHERE 0 = 0
			{}
			{}
			ORDER BY ano, p.plataforma, g.titulo
		""".format(ft_plataforma, ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorConsole(self, id_console = None):
		ft_console = ""

		if id_console:
			ft_console = " AND c.id = {}".format(id_console)

		self.cursor.execute("""
			SELECT 
				c.console, count(c.id) total
			FROM tb_consoles c
			INNER JOIN tb_games g ON g.id_console = c.id
			WHERE 0 = 0
			{}
			GROUP BY c.console
			ORDER BY total DESC
		""".format(ft_console))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorConsoleDaPlataforma(self, id_plataforma = None):
		ft_console = ""

		if id_plataforma:
			ft_console = " AND p.id = {}".format(id_plataforma)

		self.cursor.execute("""
			SELECT 
				p.plataforma, c.console, count(c.id) total
			FROM tb_consoles c
			INNER JOIN tb_plataformas p ON p.id = c.id_plataforma
			INNER JOIN tb_games g ON g.id_console = c.id
			WHERE 0 = 0
			{}
			GROUP BY p.plataforma, c.console
			ORDER BY total DESC
		""".format(ft_console))

		registros = self.cursor.fetchall()
		return registros

	def GamesPorConsoleDaPlataforma(self, id_plataforma = None):
		ft_console = ""

		if id_plataforma:
			ft_console = " AND p.id = {}".format(id_plataforma)

		self.cursor.execute("""
			SELECT 
				p.plataforma, c.console, g.titulo
			FROM tb_consoles c
			INNER JOIN tb_plataformas p ON p.id = c.id_plataforma
			INNER JOIN tb_games g ON g.id_console = c.id
			WHERE 0 = 0
			{}
			ORDER BY p.plataforma, c.console, g.titulo
		""".format(ft_console))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorPublicadora(self, ids_publicadoras = None):
		ft_publicadora = ""

		if ids_publicadoras:
			ft_publicadora = " AND s.id IN {}".format(ids_publicadoras).replace('[','(').replace(']',')')

		self.cursor.execute("""
			SELECT 
				s.studio publicadora, count(*) total
			FROM tb_games g
			INNER JOIN tb_studios s ON s.id = ANY(g.ids_publicadoras)
			WHERE 0 = 0
			{}
			GROUP BY s.studio
			ORDER BY total DESC
		""".format(ft_publicadora))

		registros = self.cursor.fetchall()
		return registros

	def GamesPorPublicadora(self, id_publicadora = None):
		ft_publicadora = ""

		if id_publicadora:
			ft_publicadora = " AND s.id = {}".format(id_publicadora)

		self.cursor.execute("""
			SELECT 
				s.studio publicadora, g.titulo
			FROM tb_games g
			INNER JOIN tb_studios s ON s.id = ANY(g.ids_publicadoras)
			WHERE 0 = 0
			{}
			ORDER BY g.titulo, publicadora
		""".format(ft_publicadora))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorDesenvolvedora(self, ids_desenvolvedoras = None):
		ft_desenvolvedora = ""

		if ids_desenvolvedoras:
			ft_desenvolvedora = " AND s.id IN {}".format(ids_desenvolvedoras).replace('[','(').replace(']',')')

		self.cursor.execute("""
			SELECT 
				s.studio desenvolvedora, count(*) total
			FROM tb_games g
			INNER JOIN tb_studios s ON s.id = ANY(g.ids_desenvolvedoras)
			WHERE 0 = 0
			{}
			GROUP BY s.studio
			ORDER BY total DESC
		""".format(ft_desenvolvedora))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorMes(self, ano = None):
		ft_ano = ""

		if ano:
			ft_ano = " AND to_char(dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				to_char(dt_finalizacao, 'MM') mes, to_char(dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_games
			WHERE 0 = 0
			{}
			GROUP BY 2, 1
			ORDER BY ano, mes
		""".format(ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def GamesPorDesenvolvedora(self, id_desenvolvedora = None):
		ft_desenvolvedora = ""

		if id_desenvolvedora:
			ft_desenvolvedora = " AND s.id = {}".format(id_desenvolvedora)

		self.cursor.execute("""
			SELECT 
				s.studio desenvolvedora, g.titulo
			FROM tb_games g
			INNER JOIN tb_studios s ON s.id = ANY(g.ids_desenvolvedoras)
			WHERE 0 = 0
			{}
			ORDER BY desenvolvedora, g.titulo
		""".format(ft_desenvolvedora))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorGeneroAoAno(self, ids_generos = None, ano = None):
		ft_genero = ""
		ft_ano = ""

		if ids_generos:
			ft_genero = " AND gn.id IN {}".format(ids_generos).replace('[','(').replace(']',')')

		if ano:
			ft_ano = " AND to_char(g.dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				gn.genero, count(*) total, to_char(g.dt_finalizacao, 'YYYY') ano
			FROM tb_games g
			INNER JOIN tb_generos gn ON gn.id = ANY(g.ids_generos)
			WHERE 0 = 0
			{}
			{}
			GROUP BY ROLLUP (gn.genero, ano)
			ORDER BY genero, ano ASC
		""".format(ft_genero, ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def GamesPorGenero(self, id_genero = None):
		ft_genero = ""

		if id_genero:
			ft_genero = " AND gn.id = {}".format(id_genero)

		self.cursor.execute("""
			SELECT 
				gn.genero, g.titulo
			FROM tb_games g
			INNER JOIN tb_generos gn ON gn.id = ANY(g.ids_generos)
			WHERE 0 = 0
			{}
			ORDER BY gn.genero, g.titulo
		""".format(ft_genero))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadeDlcXjogoBase(self):
		self.cursor.execute("""
			SELECT 
			    CASE 
			        WHEN dlc = 1 THEN 'DLC'
			        ELSE
			            'Base'
			    END AS tipo,
			    COUNT(*) total
			FROM tb_games
			GROUP BY tipo
			ORDER BY tipo
		""")

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorConsoleAoAno(self, ids_consoles = None, ano = None):
		ft_console = ""
		ft_ano = ""

		if ids_consoles:
			ft_console = " AND c.id IN {}".format(ids_consoles).replace('[','(').replace(']',')')

		if ano:
			ft_ano = " AND to_char(g.dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				c.console, to_char(g.dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_consoles c
			INNER JOIN tb_games g ON g.id_console = c.id
			WHERE 0 = 0
			{}
			{}
			GROUP BY c.console, ano
			ORDER BY ano, c.console
		""".format(ft_console, ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def GamesPorConsoleAoAno(self, id_console = None, ano = None):
		ft_console = ""
		ft_ano = ""

		if id_console:
			ft_console = " AND c.id = {}".format(id_console)

		if ano:
			ft_ano = " AND to_char(g.dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				c.console, to_char(g.dt_finalizacao, 'YYYY') ano, g.titulo
			FROM tb_consoles c
			INNER JOIN tb_games g ON g.id_console = c.id
			WHERE 0 = 0
			{}
			{}
			ORDER BY ano, c.console, g.titulo
		""".format(ft_console, ft_ano))

		registros = self.cursor.fetchall()
		return registros
	
	def ConsolidadoPorAno(self, ano = None):
		ft_ano = ""

		if ano:
			ft_ano = " AND to_char(g.dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				to_char(g.dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_games g
			WHERE 0 = 0
			{}
			GROUP BY ano
			ORDER BY ano
		""".format(ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPortatilXMesa(self, ano = None):
		ft_ano = ""

		if ano:
			ft_ano = "WHERE to_char(g.dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				to_char(g.dt_finalizacao, 'YYYY') ano,
			    CASE 
			        WHEN c.portatil = 1 THEN 'Portátil'
			        ELSE
			            'Mesa'
			    END AS tipo,
			    COUNT(*) total
			FROM tb_games g
			INNER JOIN tb_consoles c ON c.id = g.id_console
			{}
			GROUP BY ROLLUP(tipo, ano)
			ORDER BY ano
		""".format(ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPortatilXMesaPorAno(self, ano = None):
		ft_ano = ""

		if ano:
			ft_ano = " AND to_char(g.dt_finalizacao, 'YYYY') = '{}'".format(ano)
		
		self.cursor.execute("""
			SELECT
				to_char(g.dt_finalizacao, 'YYYY') ano,
			    CASE 
			        WHEN c.portatil = 1 THEN 'Portátil'
			        ELSE
			            'Mesa'
			    END AS tipo,
			    COUNT(*) total
			FROM tb_games g
			INNER JOIN tb_consoles c ON c.id = g.id_console
			WHERE 0 = 0
			{}
			GROUP BY tipo, ano
			ORDER BY ano, total DESC
		""".format(ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def ListagemGeral(self, colunas = None):
		filtros = ""

		if colunas:
			for coluna in colunas:
				if coluna['coluna'] == 'ano':
					filtros = filtros + " AND to_char(tg.dt_finalizacao, 'YYYY') = '{}'".format(coluna['valor'])
				elif coluna['coluna'] == 'desenvolvedora':
					filtros = filtros + " AND {} = ANY(tg.ids_desenvolvedoras)".format(coluna['valor'])
				elif coluna['coluna'] == 'publicadora':
					filtros = filtros + " AND {} = ANY(tg.ids_publicadoras)".format(coluna['valor'])
				elif coluna['coluna'] == 'genero':
					filtros = filtros + " AND {} = ANY(tg.ids_generos)".format(coluna['valor'])
				elif coluna['coluna'] == 'titulo':
					filtros = filtros + " AND tg.titulo ILIKE '%{}%'".format(coluna['valor'])
				elif coluna['coluna'] == 'mes':
					filtros = filtros + " AND to_char(tg.dt_finalizacao, 'MM') = '{}'".format(coluna['valor'])
				else:
					filtros = filtros + " AND {} = '{}'".format(coluna['coluna'], coluna['valor'])

		self.cursor.execute("""
			SELECT 
				tg.id,
				tg.titulo,
				CASE 
			        WHEN tg.dlc = 1 THEN 'DLC'
			        ELSE
			            'Base'
			    END AS tipo,
				tc.console,
				to_char(tg.dt_finalizacao, 'DD/MM/YYYY') AS dt,
				tp.plataforma,
				CASE 
			        WHEN tc.portatil = 1 THEN 'Sim'
			        ELSE
			            'Não'
			    END AS portatil
			FROM tb_games tg
			INNER JOIN tb_consoles tc ON tc.id = tg.id_console
			INNER JOIN tb_plataformas tp ON tp.id = tc.id_plataforma
			WHERE 0 = 0
			{}
			ORDER BY tg.dt_finalizacao, tg.titulo
		""".format(filtros))

		registros = self.cursor.fetchall()
		return registros

class TiposMidiasModel():
	def __init__(self):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)

	def Listar(self):
		self.cursor.execute("SELECT id, tipo_midia FROM tb_tipos_midias ORDER BY tipo_midia")
		registros = self.cursor.fetchall()

		return registros

class LeiturasRelatoriosModel():
	def __init__(self, id = False):
		self.cursor = conexao.cursor(cursor_factory=psycopg2.extras.DictCursor)

	def ConsolidadoPorAno(self, ano = None):
		ft_ano = ""

		if ano:
			ft_ano = " AND to_char(dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				to_char(dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_leituras
			WHERE 0 = 0
			{}
			GROUP BY 1
			ORDER BY ano
		""".format(ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorMes(self, ano = None):
		ft_ano = ""

		if ano:
			ft_ano = " AND to_char(dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				to_char(dt_finalizacao, 'MM') mes, to_char(dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_leituras
			WHERE 0 = 0
			{}
			GROUP BY 2, 1
			ORDER BY ano, mes
		""".format(ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def LeiturasPorAno(self, ano = None):
		ft_ano = ""

		if ano:
			ft_ano = " AND to_char(dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				concat(titulo, ' ', subtitulo) titulo, to_char(dt_finalizacao, 'YYYY') ano
			FROM tb_leituras
			WHERE 0 = 0
			{}
			ORDER BY ano, titulo
		""".format(ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorAutor(self, ids_autores = None):
		ft_autor = ""

		if ids_autores:
			ft_autor = " AND a.id IN {}".format(ids_autores).replace('[','(').replace(']',')')

		self.cursor.execute("""
			SELECT 
				a.autor, count(*) total
			FROM tb_leituras l
			INNER JOIN tb_autores a on a.id = ANY(l.ids_autores)
			WHERE 0 = 0
			{}
			GROUP BY a.autor
			ORDER BY total desc
		""".format(ft_autor))

		registros = self.cursor.fetchall()
		return registros

	def LeiturasPorAutor(self, ids_autores = None):
		ft_autor = ""

		if ids_autores:
			ft_autor = " AND a.id IN {}".format(ids_autores).replace('[','(').replace(']',')')

		self.cursor.execute("""
			SELECT 
				a.autor, concat(l.titulo, ' ', l.subtitulo) titulo
			FROM tb_leituras l
			INNER JOIN tb_autores a on a.id = ANY(l.ids_autores)
			WHERE 0 = 0
			{}
			ORDER BY a.autor, l.titulo
		""".format(ft_autor))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorEditora(self, ids_editoras = None):
		ft_editora = ""

		if ids_editoras:
			ft_editora = " AND e.id IN {}".format(ids_editoras).replace('[','(').replace(']',')')

		self.cursor.execute("""
			SELECT 
				e.editora, count(*) total
			FROM tb_leituras l
			inner join tb_editoras e on e.id = ANY(l.ids_editoras)
			WHERE 0 = 0
			{}
			GROUP BY e.editora
			ORDER BY total desc
		""".format(ft_editora))

		registros = self.cursor.fetchall()
		return registros

	def LeiturasPorEditora(self, id_editora = None):
		ft_editora = ""

		if id_editora:
			ft_editora = " AND e.id = '{}'".format(id_editora)

		self.cursor.execute("""
			SELECT 
				e.editora, concat(l.titulo, ' ', l.subtitulo) titulo
			FROM tb_leituras l
			inner join tb_editoras e on e.id = ANY(l.ids_editoras)
			WHERE 0 = 0
			{}
			ORDER BY editora, titulo desc
		""".format(ft_editora))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorAssuntoAoAno(self, ids_assuntos = None, ano = None):
		ft_assunto = ""
		ft_ano = ""

		if ids_assuntos:
			ft_assunto = " AND a.id IN {}".format(ids_assuntos).replace('[','(').replace(']',')')

		if ano:
			ft_ano = " AND to_char(l.dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT 
				to_char(l.dt_finalizacao, 'YYYY') ano, a.assunto, count(*) total
			FROM tb_leituras l
			INNER JOIN tb_assuntos a on a.id = ANY(l.ids_assuntos)
			WHERE 0 = 0
			{}
			{}
			GROUP BY ROLLUP(a.assunto, 1)
			ORDER BY ano, assunto ASC
		""".format(ft_assunto, ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def LeiturasPorAssunto(self, id_assunto = None):
		ft_assunto = ""

		if id_assunto:
			ft_assunto = " AND a.id = '{}'".format(id_assunto)

		self.cursor.execute("""
			SELECT 
				a.assunto, concat(l.titulo, ' ', l.subtitulo) titulo
			FROM tb_leituras l
			INNER JOIN tb_assuntos a on a.id = ANY(l.ids_assuntos)
			WHERE 0 = 0
			{}
			ORDER BY a.assunto, l.titulo ASC
		""".format(ft_assunto))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorFormatoAoAno(self, ano = None):
		ft_ano = ""
		if ano:
			ft_ano = "WHERE to_char(dt_finalizacao, 'YYYY') = '{}'".format(ano)
		
		self.cursor.execute("""
			SELECT
			    CASE 
			        WHEN digital = 1 THEN 'Digital'
			        ELSE
						'Fisíco'
			    END AS formato,
			    to_char(dt_finalizacao, 'YYYY') ano,
			    count(*) total
			FROM tb_leituras
			{}
			GROUP BY formato, 2
			ORDER BY 2, formato
		""".format(ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def LeiturasPorFormato(self, formato = None):
		ft_formato = ""

		if formato:
			ft_formato = " AND l.digital = '{}'".format(formato)

		self.cursor.execute("""
			SELECT
			    concat(l.titulo, ' ', l.subtitulo) titulo,
			    CASE 
			        WHEN digital = 1 THEN 'Digital'
			        ELSE
						'Fisíco'
			    END AS formato
			FROM tb_leituras l
			WHERE 0 = 0
			{}
			ORDER BY formato, titulo
		""".format(ft_formato))

		registros = self.cursor.fetchall()
		return registros

	def ConsolidadoPorTipoMidiaAoAno(self, ids_tipos = None, ano = None):
		ft_tipo = ""
		ft_ano = ""

		if ids_tipos:
			ft_tipo = " AND l.id_tipo_midia IN {}".format(ids_tipos).replace('[','(').replace(']',')')
		if ano:
			ft_ano = " AND to_char(l.dt_finalizacao, 'YYYY') = '{}'".format(ano)

		self.cursor.execute("""
			SELECT
			    tm.tipo_midia, to_char(l.dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_leituras l
			INNER JOIN tb_tipos_midias tm ON tm.id = l.id_tipo_midia
			WHERE 0 = 0
			{}
			{}
			GROUP BY tm.tipo_midia, 2
			ORDER BY 2, total DESC
		""".format(ft_tipo, ft_ano))

		registros = self.cursor.fetchall()
		return registros

	def LeiturasPorTipo(self, id_tipo = None):
		ft_tipo = ""

		if id_tipo:
			ft_tipo = " AND l.id_tipo_midia = '{}'".format(id_tipo)

		self.cursor.execute("""
			SELECT
			    concat(l.titulo, ' ', l.subtitulo) titulo, tm.tipo_midia tipo
			FROM tb_leituras l
			INNER JOIN tb_tipos_midias tm ON tm.id = l.id_tipo_midia
			WHERE 0 = 0
			{}
			ORDER BY tm.tipo_midia, l.titulo
		""".format(ft_tipo))

		registros = self.cursor.fetchall()
		return registros

	def ListagemGeral(self, colunas = None):
		filtros = ""

		if colunas:
			for coluna in colunas:
				if coluna['coluna'] == 'ano':
					filtros = filtros + " AND to_char(l.dt_finalizacao, 'YYYY') = '{}'".format(coluna['valor'])
				elif coluna['coluna'] == 'assunto':
					filtros = filtros + " AND {} = ANY(l.ids_assuntos)".format(coluna['valor'])
				elif coluna['coluna'] == 'autor':
					filtros = filtros + " AND {} = ANY(l.ids_autores)".format(coluna['valor'])
				elif coluna['coluna'] == 'titulo':
					filtros = filtros + " AND l.titulo ILIKE '%{}%'".format(coluna['valor'])
				elif coluna['coluna'] == 'mes':
					filtros = filtros + " AND to_char(l.dt_finalizacao, 'MM') = '{}'".format(coluna['valor'])
				else:
					filtros = filtros + " AND {} = '{}'".format(coluna['coluna'], coluna['valor'])

		self.cursor.execute("""
			SELECT
				l.id, 
				concat(l.titulo, ' ', l.subtitulo) titulo,
				e.editora, 
				tp.tipo_midia tipo, 
				to_char(l.dt_finalizacao, 'DD/MM/YYYY') as dt,
			    CASE 
			        WHEN digital = 1 THEN 'Digital'
			        ELSE
						'Fisíco'
			    END AS formato
			FROM tb_leituras l
			INNER JOIN tb_editoras e on e.id = any(l.ids_editoras)
			INNER JOIN tb_tipos_midias tp on tp.id = l.id_tipo_midia
			WHERE 0 = 0
			{}
			ORDER BY l.dt_finalizacao, l.titulo DESC
		""".format(filtros))

		registros = self.cursor.fetchall()
		return registros