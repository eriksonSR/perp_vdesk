import gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from models.models import *

class WinListaAssuntos:
	def __init__(self):
		self.handlers = {
			"busca_assunto": self.BuscaAssunto
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/assuntos/win_lista_assuntos.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_assuntos = AssuntosModel()
		self.list_assuntos = Gtk.ListStore(int, str, str)
		self.tree_assuntos = self.builder.get_object('treeview_assuntos')
		self.ExibeLista()
		self.win_lista_assunto = self.builder.get_object("win_lista_assuntos")
		self.win_lista_assunto.connect("destroy", Gtk.main_quit)
		self.win_lista_assunto.show_all()

		Gtk.main()

	def ExibeLista(self):
		assuntos = self.m_assuntos.Listar()
		for assunto in assuntos:
			self.list_assuntos.append([assunto['id'], assunto['assunto'], 'document-edit'])

		self.tree_assuntos.set_model(self.list_assuntos)
		
		renderer_id = Gtk.CellRendererText()
		column_id = Gtk.TreeViewColumn("ID", renderer_id, text=0)
		self.tree_assuntos.append_column(column_id)
		
		renderer_editora = Gtk.CellRendererText()
		column_autor = Gtk.TreeViewColumn("Assunto", renderer_editora, text=1)
		self.tree_assuntos.append_column(column_autor)

		renderer_editar = Gtk.CellRendererPixbuf()
		renderer_editar.set_property('xalign', 0.0)
		column_editar = Gtk.TreeViewColumn("Editar", renderer_editar, icon_name=2)
		self.tree_assuntos.append_column(column_editar)

		self.tree_assuntos.set_activate_on_single_click(True)
		self.tree_assuntos.connect("row_activated", self.EditarAssunto)

	def EditarAssunto(self, treeview, path, column):
		if column.get_title() == 'Editar':
			model, treeiter = treeview.get_selection().get_selected()
			id_assunto = model[treeiter][0]
			win_edita_assunto = WinEditaAssunto(id_assunto)

	def BuscaAssunto(self, widget):
		entry = self.builder.get_object('entry_assunto')
		assunto = entry.get_text()
		qtd_chars = len(assunto)
		
		if qtd_chars >= 3:
			self.list_assuntos.clear()
			assuntos = self.m_assuntos.GetAssuntoByNome(assunto)

			for assunto in assuntos:
				self.list_assuntos.append([assunto['id'], assunto['assunto'], 'document-edit'])
			
			self.tree_assuntos.set_model(self.list_assuntos)
		elif qtd_chars == 0:
			self.list_assuntos.clear()
			assuntos = self.m_assuntos.Listar()

			for assunto in assuntos:
				self.list_assuntos.append([assunto['id'], assunto['assunto'], 'document-edit'])
			
			self.tree_assuntos.set_model(self.list_assuntos)

class WinEditaAssunto:
	def __init__(self, id_assunto):
		self.id_assunto = id_assunto
		self.handlers = {
			"salvar_assunto": self.SalvaAssunto
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/assuntos/win_edita_assunto.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_edita_assunto = self.builder.get_object("win_edita_assunto")
		self.win_edita_assunto.connect("destroy", Gtk.main_quit)
		self.win_edita_assunto.show_all()
		self.GetAssunto()

		Gtk.main()

	def SalvaAssunto(self, widget):
		entry = self.builder.get_object('entry_assunto')
		assunto_nome = entry.get_text()
		assunto = {'assunto':assunto_nome, 'id':self.id_assunto}
		m_assunto = AssuntosModel()
		if not m_assunto.TestaSeExisteAssunto(assunto_nome):
			m_assunto.Atualizar(assunto)
			self.Info('sucesso', 'Assunto atualizada com sucesso!')
		else:
			self.Info('erro', 'Assunto já cadastrada!')

	def GetAssunto(self):
		m_assunto = AssuntosModel()
		assunto = m_assunto.GetAssuntoById(self.id_assunto)

		entry = self.builder.get_object('entry_assunto')
		entry.set_text(assunto['assunto'])

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()

class WinAdicionaAssunto:
	def __init__(self):
		self.handlers = {
			"add_assunto": self.AdicionaAssunto
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/assuntos/win_add_assunto.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_add_assunto = self.builder.get_object("win_add_assunto")
		self.win_add_assunto.connect("destroy", Gtk.main_quit)
		self.win_add_assunto.show_all()

		Gtk.main()

	def AdicionaAssunto(self, widget):
		entry = self.builder.get_object('entry_assunto')
		assunto = entry.get_text()
		m_assunto = AssuntosModel()
		if not m_assunto.TestaSeExisteAssunto(assunto):
			m_assunto.Inserir(assunto)
			entry.set_text('')
			self.Info('sucesso', 'Assunto cadastrado com sucesso!')
		else:
			self.Info('erro', 'Assunto já cadastrado!')

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()