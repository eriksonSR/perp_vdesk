import gi, json, requests, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from models.models import *
from games.games import *
from leituras.leituras import *
from pprint import pprint

class WinRelDlcXjogoBase():
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/games/win_rel_dlc_x_jogo_base.glade'))
		self.m_relatorios = GamesRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_dlc_x_jogo_base')
		self.ExibeRelatorio()
		self.win_lista_autor = self.builder.get_object("win_rel_dlc_x_jogo_base")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadeDlcXjogoBase()
		for dado in dados:
			self.list_relatorio.append([dado['tipo'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_tipo = Gtk.CellRendererText()
		column_tipo = Gtk.TreeViewColumn("Tipo", renderer_tipo, text=0)
		self.tree_relatorio.append_column(column_tipo)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=1)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=2)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'dlc', 'valor':0}]

			model, treeiter = treeview.get_selection().get_selected()
			tipo = model[treeiter][0]
			if tipo == 'DLC':
				colunas[0]['valor'] = 1

			WinListaGames(colunas)

class WinRelConsolidadoPorAno():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/games/win_rel_consolidado_por_ano.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = GamesRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_consolidado_por_ano')
		self.ExibeRelatorio()
		self.win_lista_autor = self.builder.get_object("win_rel_consolidado_por_ano")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorAno()
		
		for dado in dados:
			self.list_relatorio.append([dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Ano", renderer_ano, text=0)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=1)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=2)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		ano = self.builder.get_object('entry_ano').get_text()
		qtd_chars = len(ano)
		dados = ''
		
		if qtd_chars == 4:
			dados = self.m_relatorios.ConsolidadoPorAno(ano)
		elif qtd_chars == 0:
			dados = self.m_relatorios.ConsolidadoPorAno()
		
		for dado in dados:
			self.list_relatorio.append([dado['ano'], dado['total']])

		self.tree_relatorio.set_model(self.list_relatorio)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0}]

			model, treeiter = treeview.get_selection().get_selected()
			ano = model[treeiter][0]
			colunas[0]['valor'] = ano

			WinListaGames(colunas)

class WinRelConsolidadoPorGenero():
	def __init__(self):
		self.generos_marcados = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/games/win_rel_consolidado_por_genero.glade'))
		self.m_relatorios = GamesRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, int)
		self.tree_relatorio = self.builder.get_object('treeview_rel_consolidado_por_genero')
		self.ExibeRelatorio()
		self.win_lista_autor = self.builder.get_object("win_rel_consolidado_por_genero")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorGenero()
		
		for dado in dados:
			self.list_relatorio.append([dado['genero'], dado['total']])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_genero = Gtk.CellRendererText()
		column_genero = Gtk.TreeViewColumn("Gênero", renderer_genero, text=0)
		self.tree_relatorio.append_column(column_genero)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=1)
		self.tree_relatorio.append_column(column_total)

class WinRelConsolidadoPorGeneroAoAno():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.generos_marcados = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/games/win_rel_consolidado_por_genero_ao_ano.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = GamesRelatoriosModel()
		self.m_generos = GenerosModel()
		self.list_relatorio = Gtk.ListStore(str, str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_consolidado_por_genero_ao_ano')
		self.ExibeRelatorio()
		self.PreencheGeneros()
		self.win_lista_autor = self.builder.get_object("win_rel_consolidado_por_genero_ao_ano")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def PreencheGeneros(self):
		m_generos = GenerosModel()
		generos = m_generos.Listar()
		listbox = Gtk.ListBox()
		
		for genero in generos:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(genero['genero'])
			ckbutton.connect("toggled", self.OnGeneroToggled, genero['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_generos')
		scroll.add(listbox)

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorGeneroAoAno()
		
		for dado in dados:
			if dado['ano'] == None:
				self.list_relatorio.append([dado['genero'], dado['ano'], dado['total'], ''])
			else:
				self.list_relatorio.append([dado['genero'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_genero = Gtk.CellRendererText()
		column_genero = Gtk.TreeViewColumn("Gênero", renderer_genero, text=0)
		self.tree_relatorio.append_column(column_genero)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Gênero", renderer_ano, text=1)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=2)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=3)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		ano = self.builder.get_object('entry_ano').get_text()
		
		self.list_relatorio.clear()
		dados = self.m_relatorios.ConsolidadoPorGeneroAoAno(self.generos_marcados, ano)

		for dado in dados:
			if dado['ano'] == None:
				self.list_relatorio.append([dado['genero'], dado['ano'], dado['total'], ''])
			else:
				self.list_relatorio.append([dado['genero'], dado['ano'], dado['total'], 'edit-find'])
			
		self.tree_relatorio.set_model(self.list_relatorio)

	def OnGeneroToggled(self, button, id_genero):
		if button.get_active():
			if id_genero not in self.generos_marcados:
				self.generos_marcados.append(id_genero)
		else:
			self.generos_marcados.remove(id_genero)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0},{'coluna':'genero', 'valor':''}]

			model, treeiter = treeview.get_selection().get_selected()
			ano = model[treeiter][1]
			colunas[0]['valor'] = ano

			genero = model[treeiter][0]
			id_genero = self.m_generos.GetGenerosByNome(genero)[0]['id']
			colunas[1]['valor'] = id_genero
			WinListaGames(colunas)

class WinRelConsolidadoPorDesenvolvedora():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.studios_marcados = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/games/win_rel_consolidado_por_desenvolvedora.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = GamesRelatoriosModel()
		self.m_studios = StudiosModel()
		self.list_relatorio = Gtk.ListStore(str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_consolidado_por_desenvolvedora')
		self.ExibeRelatorio()
		self.PreencheStudios()
		self.win_lista_autor = self.builder.get_object("win_rel_consolidado_por_desenvolvedora")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorDesenvolvedora()
		
		for dado in dados:
			self.list_relatorio.append([dado['desenvolvedora'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_desenvolvedora = Gtk.CellRendererText()
		column_desenvolvedora = Gtk.TreeViewColumn("Desenvolvedora", renderer_desenvolvedora, text=0)
		self.tree_relatorio.append_column(column_desenvolvedora)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=1)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=2)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		dados = self.m_relatorios.ConsolidadoPorDesenvolvedora(self.studios_marcados)

		for dado in dados:
			self.list_relatorio.append([dado['desenvolvedora'], dado['total'], 'edit-find'])
			
		self.tree_relatorio.set_model(self.list_relatorio)

	def PreencheStudios(self):
		studios = self.m_studios.Listar()
		listbox = Gtk.ListBox()
		
		for studio in studios:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(studio['studio'])
			ckbutton.connect("toggled", self.OnGeneroToggled, studio['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_studios')
		scroll.add(listbox)

	def OnGeneroToggled(self, button, id_studio):
		if button.get_active():
			if id_studio not in self.studios_marcados:
				self.studios_marcados.append(id_studio)
		else:
			self.studios_marcados.remove(id_studio)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'desenvolvedora', 'valor':''}]

			model, treeiter = treeview.get_selection().get_selected()
			desenvolvedora = model[treeiter][0]
			id_desenvolvedora = self.m_studios.GetStudiosByNome(desenvolvedora)[0]['id']
			colunas[0]['valor'] = id_desenvolvedora

			WinListaGames(colunas)

class WinRelConsolidadoPorPublicadora():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.studios_marcados = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/games/win_rel_consolidado_por_publicadora.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = GamesRelatoriosModel()
		self.m_publicadoras = StudiosModel()
		self.list_relatorio = Gtk.ListStore(str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_consolidado_por_publicadora')
		self.ExibeRelatorio()
		self.PreencheStudios()
		self.win_lista_autor = self.builder.get_object("win_rel_consolidado_por_publicadora")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorPublicadora()
		
		for dado in dados:
			self.list_relatorio.append([dado['publicadora'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_desenvolvedora = Gtk.CellRendererText()
		column_desenvolvedora = Gtk.TreeViewColumn("Publicadora", renderer_desenvolvedora, text=0)
		self.tree_relatorio.append_column(column_desenvolvedora)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=1)
		self.tree_relatorio.append_column(column_total)

		renderer_publicadora = Gtk.CellRendererPixbuf()
		renderer_publicadora.set_property('xalign', 0.0)
		column_publicadora = Gtk.TreeViewColumn("Visualizar", renderer_publicadora, icon_name=2)
		self.tree_relatorio.append_column(column_publicadora)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		dados = self.m_relatorios.ConsolidadoPorPublicadora(self.studios_marcados)

		for dado in dados:
			self.list_relatorio.append([dado['publicadora'], dado['total'], 'edit-find'])
			
		self.tree_relatorio.set_model(self.list_relatorio)

	def PreencheStudios(self):
		m_studios = StudiosModel()
		studios = m_studios.Listar()
		listbox = Gtk.ListBox()
		
		for studio in studios:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(studio['studio'])
			ckbutton.connect("toggled", self.OnGeneroToggled, studio['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_studios')
		scroll.add(listbox)

	def OnGeneroToggled(self, button, id_studio):
		if button.get_active():
			if id_studio not in self.studios_marcados:
				self.studios_marcados.append(id_studio)
		else:
			self.studios_marcados.remove(id_studio)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'publicadora', 'valor':''}]

			model, treeiter = treeview.get_selection().get_selected()
			publicadora = model[treeiter][0]
			id_publicadora = self.m_publicadoras.GetStudiosByNome(publicadora)[0]['id']
			colunas[0]['valor'] = id_publicadora

			WinListaGames(colunas)

class WinRelConsolidadoPorConsoleAoAno():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.consoles_marcados = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/games/win_rel_consolidado_por_console_ao_ano.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = GamesRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_consolidado_por_console_ao_ano')
		self.ExibeRelatorio()
		self.PreencheConsoles()
		self.win_lista_autor = self.builder.get_object("win_rel_consolidado_por_console_ao_ano")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def PreencheConsoles(self):
		m_consoles = ConsolesModel()
		consoles = m_consoles.Listar()
		listbox = Gtk.ListBox()
		
		for console in consoles:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(console['console'])
			ckbutton.connect("toggled", self.OnGeneroToggled, console['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_consoles')
		scroll.add(listbox)

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorConsoleAoAno()
		
		for dado in dados:
			self.list_relatorio.append([dado['console'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_console = Gtk.CellRendererText()
		column_console = Gtk.TreeViewColumn("Console", renderer_console, text=0)
		self.tree_relatorio.append_column(column_console)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Ano", renderer_ano, text=1)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=2)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=3)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)


	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0},{'coluna':'console', 'valor':''}]

			model, treeiter = treeview.get_selection().get_selected()
			ano = model[treeiter][1]
			colunas[0]['valor'] = ano
			
			console = model[treeiter][0]
			colunas[1]['valor'] = console

			WinListaGames(colunas)

	def AplicarFiltro(self, widget):
		ano = self.builder.get_object('entry_ano').get_text()
		
		self.list_relatorio.clear()
		dados = self.m_relatorios.ConsolidadoPorConsoleAoAno(self.consoles_marcados, ano)

		for dado in dados:
			self.list_relatorio.append([dado['console'], dado['ano'], dado['total'], 'edit-find'])
			
		self.tree_relatorio.set_model(self.list_relatorio)

	def OnGeneroToggled(self, button, id_console):
		if button.get_active():
			if id_console not in self.consoles_marcados:
				self.consoles_marcados.append(id_console)
		else:
			self.consoles_marcados.remove(id_console)

class WinRelConsolidadoPorPlataformaAoAno():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.plataformas_marcadas = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/games/win_rel_consolidado_por_plataforma_ao_ano.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = GamesRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_consolidado_por_plataforma_ao_ano')
		self.ExibeRelatorio()
		self.PreenchePlataformas()
		self.win_lista_autor = self.builder.get_object("win_rel_consolidado_por_plataforma_ao_ano")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def PreenchePlataformas(self):
		m_plataformas = PlataformasModel()
		plataformas = m_plataformas.Listar()
		listbox = Gtk.ListBox()
		
		for plataforma in plataformas:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(plataforma['plataforma'])
			ckbutton.connect("toggled", self.OnPlataformaToggled, plataforma['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_plataformas')
		scroll.add(listbox)

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorPlataformaAoAno()
		
		for dado in dados:
			self.list_relatorio.append([dado['plataforma'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_plataforma = Gtk.CellRendererText()
		column_plataforma = Gtk.TreeViewColumn("Plataforma", renderer_plataforma, text=0)
		self.tree_relatorio.append_column(column_plataforma)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Ano", renderer_ano, text=1)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=2)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=3)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		ano = self.builder.get_object('entry_ano').get_text()
		
		self.list_relatorio.clear()
		dados = self.m_relatorios.ConsolidadoPorPlataformaAoAno(self.plataformas_marcadas, ano)

		for dado in dados:
			self.list_relatorio.append([dado['plataforma'], dado['ano'], dado['total'], 'edit-find'])
			
		self.tree_relatorio.set_model(self.list_relatorio)

	def OnPlataformaToggled(self, button, id_plataforma):
		if button.get_active():
			if id_plataforma not in self.plataformas_marcadas:
				self.plataformas_marcadas.append(id_plataforma)
		else:
			self.plataformas_marcadas.remove(id_plataforma)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0},{'coluna':'plataforma', 'valor':''}]

			model, treeiter = treeview.get_selection().get_selected()
			ano = model[treeiter][1]
			colunas[0]['valor'] = ano
			
			genero = model[treeiter][0]
			colunas[1]['valor'] = genero

			WinListaGames(colunas)

class WinRelConsolidadoPortatilXmesa():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/games/win_relatorio_consolidado_portatil_x_mesa.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = GamesRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_consolidado_portatil_x_mesa')
		self.ExibeRelatorio()
		self.win_lista_autor = self.builder.get_object("win_relatorio_consolidado_portatil_x_mesa")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPortatilXMesa()
		
		for dado in dados:
			if dado['ano'] == None or dado['tipo'] == None:
				self.list_relatorio.append([dado['tipo'], dado['ano'], dado['total'], ''])
			else:
				self.list_relatorio.append([dado['tipo'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_tipo = Gtk.CellRendererText()
		column_tio = Gtk.TreeViewColumn("Tipo", renderer_tipo, text=0)
		self.tree_relatorio.append_column(column_tio)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Ano", renderer_ano, text=1)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=2)
		self.tree_relatorio.append_column(column_total)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=3)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		ano = self.builder.get_object('entry_ano').get_text()
		self.list_relatorio.clear()
		dados = self.m_relatorios.ConsolidadoPortatilXMesa(ano)

		for dado in dados:
			self.list_relatorio.append([dado['tipo'], dado['ano'], dado['total'], 'edit-find'])
			
		self.tree_relatorio.set_model(self.list_relatorio)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0},{'coluna':'portatil', 'valor':0}]

			model, treeiter = treeview.get_selection().get_selected()
			if model[treeiter][1] == None:
				return

			portatil = model[treeiter][0]
			if portatil == 'Portátil':
				colunas[1]['valor'] = 1

			ano = model[treeiter][1]
			colunas[0]['valor'] = ano

			WinListaGames(colunas)

class WinRelLeiturasConsolidadoPorAno():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/leituras/win_rel_leituras_consolidado_por_ano.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = LeiturasRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_leituras_consolidado_por_ano')
		self.ExibeRelatorio()
		self.win_lista_autor = self.builder.get_object("win_rel_leituras_consolidado_por_ano")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorAno()
		
		for dado in dados:
			self.list_relatorio.append([dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Ano", renderer_ano, text=0)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=1)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=2)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		ano = self.builder.get_object('entry_ano').get_text()
		qtd_chars = len(ano)
		dados = ''
		
		if qtd_chars == 4:
			dados = self.m_relatorios.ConsolidadoPorAno(ano)
		elif qtd_chars == 0:
			dados = self.m_relatorios.ConsolidadoPorAno()
		
		for dado in dados:
			self.list_relatorio.append([dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0}]

			model, treeiter = treeview.get_selection().get_selected()
			ano = model[treeiter][0]
			colunas[0]['valor'] = ano

			WinListaLeituras(colunas)

class WinRelLeiturasConsolidadoPorAssuntoAoAno():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.assuntos_marcados = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/leituras/win_rel_leituras_consolidado_por_assunto_ao_ano.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = LeiturasRelatoriosModel()
		self.m_assuntos = AssuntosModel()
		self.list_relatorio = Gtk.ListStore(str, str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_leituras_consolidado_por_assunto_ao_ano')
		self.PreencheAssuntos()
		self.ExibeRelatorio()
		self.win_lista_autor = self.builder.get_object("win_rel_leituras_consolidado_por_assunto_ao_ano")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorAssuntoAoAno()
		
		for dado in dados:
			if dado['ano'] == None:
				self.list_relatorio.append([dado['assunto'], dado['ano'], dado['total'], ''])
			else:
				self.list_relatorio.append([dado['assunto'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_assunto = Gtk.CellRendererText()
		column_assunto = Gtk.TreeViewColumn("Assunto", renderer_assunto, text=0)
		self.tree_relatorio.append_column(column_assunto)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Ano", renderer_ano, text=1)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=2)
		self.tree_relatorio.append_column(column_total)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=3)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		ano = self.builder.get_object('entry_ano').get_text()
		qtd_chars = len(ano)
		dados = ''
		
		if qtd_chars == 4:
			dados = self.m_relatorios.ConsolidadoPorAssuntoAoAno(self.assuntos_marcados, ano)
		elif qtd_chars == 0:
			dados = self.m_relatorios.ConsolidadoPorAssuntoAoAno(self.assuntos_marcados)
		
		for dado in dados:
			if dado['ano'] == None:
				self.list_relatorio.append([dado['assunto'], dado['ano'], dado['total'], ''])
			else:
				self.list_relatorio.append([dado['assunto'], dado['ano'], dado['total'], 'edit-find'])
 
		self.tree_relatorio.set_model(self.list_relatorio)

	def PreencheAssuntos(self):
		m_assuntos = AssuntosModel()
		assuntos = m_assuntos.Listar()
		listbox = Gtk.ListBox()
		
		for assunto in assuntos:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(assunto['assunto'])
			ckbutton.connect("toggled", self.OnAssuntoToggled, assunto['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_assuntos')
		scroll.add(listbox)

	def OnAssuntoToggled(self, button, id_assunto):
		if button.get_active():
			if id_assunto not in self.assuntos_marcados:
				self.assuntos_marcados.append(id_assunto)
		else:
			self.assuntos_marcados.remove(id_assunto)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0},{'coluna':'assunto', 'valor':''}]

			model, treeiter = treeview.get_selection().get_selected()
			ano = model[treeiter][1]
			colunas[0]['valor'] = ano
			
			assunto = model[treeiter][0]
			id_assunto = self.m_assuntos.GetAssuntoByNome(assunto)[0]['id']
			colunas[1]['valor'] = id_assunto

			WinListaLeituras(colunas)

class WinRelLeiturasConsolidadoPorAutor():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.autores_marcados = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/leituras/win_rel_leituras_consolidado_por_autor.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = LeiturasRelatoriosModel()
		self.m_autores = AutoresModel()
		self.list_relatorio = Gtk.ListStore(str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_leituras_consolidado_por_autor')
		self.ExibeRelatorio()
		self.PreencheAutores()
		self.win_lista_autor = self.builder.get_object("win_rel_leituras_consolidado_por_autor")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorAutor()
		
		for dado in dados:
			self.list_relatorio.append([dado['autor'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_autor = Gtk.CellRendererText()
		column_autor = Gtk.TreeViewColumn("Autor", renderer_autor, text=0)
		self.tree_relatorio.append_column(column_autor)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=1)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=2)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		dados = self.m_relatorios.ConsolidadoPorAutor(self.autores_marcados)

		for dado in dados:
			self.list_relatorio.append([dado['autor'], dado['total'], 'edit-find'])
			
		self.tree_relatorio.set_model(self.list_relatorio)

	def PreencheAutores(self):
		m_autores = AutoresModel()
		autores = m_autores.Listar()
		listbox = Gtk.ListBox()
		
		for autor in autores:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(autor['autor'])
			ckbutton.connect("toggled", self.OnAutorToggled, autor['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_autores')
		scroll.add(listbox)

	def OnAutorToggled(self, button, id_autor):
		if button.get_active():
			if id_autor not in self.autores_marcados:
				self.autores_marcados.append(id_autor)
		else:
			self.autores_marcados.remove(id_autor)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'autor', 'valor':''}]

			model, treeiter = treeview.get_selection().get_selected()
			autor = model[treeiter][0]
			id_autor = self.m_autores.GetAutorByNome(autor)[0]['id']
			colunas[0]['valor'] = id_autor
			WinListaLeituras(colunas)

class WinRelLeiturasConsolidadoPorEditora():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.editoras_marcadas = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/leituras/win_rel_leituras_consolidado_por_editora.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = LeiturasRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_leituras_consolidado_por_editora')
		self.ExibeRelatorio()
		self.PreencheEditoras()
		self.win_lista_autor = self.builder.get_object("win_rel_leituras_consolidado_por_editora")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorEditora()
		
		for dado in dados:
			self.list_relatorio.append([dado['editora'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_editora = Gtk.CellRendererText()
		column_editora = Gtk.TreeViewColumn("Editora", renderer_editora, text=0)
		self.tree_relatorio.append_column(column_editora)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=1)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=2)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		dados = self.m_relatorios.ConsolidadoPorEditora(self.editoras_marcadas)

		for dado in dados:
			self.list_relatorio.append([dado['editora'], dado['total'], 'edit-find'])
			
		self.tree_relatorio.set_model(self.list_relatorio)

	def PreencheEditoras(self):
		m_editoras = EditorasModel()
		editoras = m_editoras.Listar()
		listbox = Gtk.ListBox()
		
		for editora in editoras:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(editora['editora'])
			ckbutton.connect("toggled", self.OnEditoraToggled, editora['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_editoras')
		scroll.add(listbox)

	def OnEditoraToggled(self, button, id_editora):
		if button.get_active():
			if id_editora not in self.editoras_marcadas:
				self.editoras_marcadas.append(id_editora)
		else:
			self.editoras_marcadas.remove(id_editora)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'editora', 'valor':''}]

			model, treeiter = treeview.get_selection().get_selected()
			editora = model[treeiter][0]
			colunas[0]['valor'] = editora
			WinListaLeituras(colunas)

class WinRelLeiturasConsolidadoPorFormatoAoAno():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.editoras_marcadas = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/leituras/win_rel_leituras_consolidado_por_formato_ao_ano.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = LeiturasRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_leituras_consolidado_por_formato_ao_ano')
		self.ExibeRelatorio()
		self.win_lista_autor = self.builder.get_object("win_rel_leituras_consolidado_por_formato_ao_ano")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorFormatoAoAno()
		
		for dado in dados:
			self.list_relatorio.append([dado['formato'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_formato = Gtk.CellRendererText()
		column_formato = Gtk.TreeViewColumn("Formato", renderer_formato, text=0)
		self.tree_relatorio.append_column(column_formato)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Ano", renderer_ano, text=1)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=2)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=3)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		ano = self.builder.get_object('entry_ano').get_text()
		qtd_chars = len(ano)
		dados = ''
		
		if qtd_chars == 4:
			dados = self.m_relatorios.ConsolidadoPorFormatoAoAno(ano)
		elif qtd_chars == 0:
			dados = self.m_relatorios.ConsolidadoPorFormatoAoAno()
		
		for dado in dados:
			self.list_relatorio.append([dado['formato'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0},{'coluna':'digital', 'valor':0}]

			model, treeiter = treeview.get_selection().get_selected()
			ano = model[treeiter][1]
			colunas[0]['valor'] = ano
			
			formato = model[treeiter][0]
			if formato == 'Digital':
				colunas[1]['valor'] = 1

			WinListaLeituras(colunas)

class WinRelLeiturasConsolidadoPorTipoMidiaAoAno():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.tipos_midias_marcados = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/leituras/win_rel_leituras_consolidado_por_tipo_midia_ao_ano.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = LeiturasRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_leituras_consolidado_por_tipo_midia_ao_ano')
		self.PreencheTipos()
		self.ExibeRelatorio()
		self.win_lista_autor = self.builder.get_object("win_rel_leituras_consolidado_por_tipo_midia_ao_ano")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorTipoMidiaAoAno()
		
		for dado in dados:
			self.list_relatorio.append([dado['tipo_midia'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_tipo = Gtk.CellRendererText()
		column_tipo = Gtk.TreeViewColumn("Tipo", renderer_tipo, text=0)
		self.tree_relatorio.append_column(column_tipo)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Ano", renderer_ano, text=1)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=2)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=3)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		ano = self.builder.get_object('entry_ano').get_text()
		qtd_chars = len(ano)
		dados = ''
		
		if qtd_chars == 4:
			dados = self.m_relatorios.ConsolidadoPorTipoMidiaAoAno(self.tipos_midias_marcados, ano)
		elif qtd_chars == 0:
			dados = self.m_relatorios.ConsolidadoPorTipoMidiaAoAno(self.tipos_midias_marcados)
		
		for dado in dados:
			self.list_relatorio.append([dado['tipo_midia'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)

	def PreencheTipos(self):
		m_tipos = TiposMidiasModel()
		tipos = m_tipos.Listar()
		listbox = Gtk.ListBox()
		
		for tipo in tipos:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(tipo['tipo_midia'])
			ckbutton.connect("toggled", self.OnTipoMidiaToggled, tipo['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_tipos_midias')
		scroll.add(listbox)

	def OnTipoMidiaToggled(self, button, id_tipo_midia):
		if button.get_active():
			if id_tipo_midia not in self.tipos_midias_marcados:
				self.tipos_midias_marcados.append(id_tipo_midia)
		else:
			self.tipos_midias_marcados.remove(id_tipo_midia)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0},{'coluna':'tipo_midia', 'valor':''}]

			model, treeiter = treeview.get_selection().get_selected()
			ano = model[treeiter][1]
			colunas[0]['valor'] = ano
			
			tipo = model[treeiter][0]
			colunas[1]['valor'] = tipo

			WinListaLeituras(colunas)

class WinRelLeiturasConsolidadoPorMes():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/leituras/win_rel_leituras_consolidado_por_mes.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = LeiturasRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_leituras_consolidado_por_mes')
		self.ExibeRelatorio()
		self.win_rel = self.builder.get_object("win_rel_leituras_consolidado_por_mes")
		self.win_rel.connect("destroy", Gtk.main_quit)
		self.win_rel.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorMes()
		
		for dado in dados:
			self.list_relatorio.append([dado['mes'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_mes = Gtk.CellRendererText()
		column_mes = Gtk.TreeViewColumn("Mês", renderer_mes, text=0)
		self.tree_relatorio.append_column(column_mes)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Ano", renderer_ano, text=1)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=2)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=3)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		ano = self.builder.get_object('entry_ano').get_text()
		qtd_chars = len(ano)
		dados = ''
		
		if qtd_chars == 4:
			dados = self.m_relatorios.ConsolidadoPorMes(ano)
		elif qtd_chars == 0:
			dados = self.m_relatorios.ConsolidadoPorMes()
		
		for dado in dados:
			self.list_relatorio.append([dado['mes'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0},{'coluna':'mes', 'valor':0}]

			model, treeiter = treeview.get_selection().get_selected()
			mes = model[treeiter][0]
			colunas[1]['valor'] = mes

			model, treeiter = treeview.get_selection().get_selected()
			ano = model[treeiter][1]
			colunas[0]['valor'] = ano

			WinListaLeituras(colunas)

class WinRelGamesConsolidadoPorMes():
	def __init__(self):
		self.handlers = {
			"filtrar": self.AplicarFiltro
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/relatorios/games/win_rel_consolidado_por_mes.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_relatorios = GamesRelatoriosModel()
		self.list_relatorio = Gtk.ListStore(str, str, int, str)
		self.tree_relatorio = self.builder.get_object('treeview_rel_consolidado_por_mes')
		self.ExibeRelatorio()
		self.win_rel = self.builder.get_object("win_rel_consolidado_por_mes")
		self.win_rel.connect("destroy", Gtk.main_quit)
		self.win_rel.show_all()

		Gtk.main()

	def ExibeRelatorio(self):
		dados = self.m_relatorios.ConsolidadoPorMes()
		
		for dado in dados:
			self.list_relatorio.append([dado['mes'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)
		
		renderer_mes = Gtk.CellRendererText()
		column_mes = Gtk.TreeViewColumn("Mês", renderer_mes, text=0)
		self.tree_relatorio.append_column(column_mes)
		
		renderer_ano = Gtk.CellRendererText()
		column_ano = Gtk.TreeViewColumn("Ano", renderer_ano, text=1)
		self.tree_relatorio.append_column(column_ano)
		
		renderer_total = Gtk.CellRendererText()
		column_total = Gtk.TreeViewColumn("Total", renderer_total, text=2)
		self.tree_relatorio.append_column(column_total)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=3)
		self.tree_relatorio.append_column(column_visualizar)

		self.tree_relatorio.set_activate_on_single_click(True)
		self.tree_relatorio.connect("row_activated", self.VisualizarDados)

	def AplicarFiltro(self, widget):
		self.list_relatorio.clear()
		ano = self.builder.get_object('entry_ano').get_text()
		qtd_chars = len(ano)
		dados = ''
		
		if qtd_chars == 4:
			dados = self.m_relatorios.ConsolidadoPorMes(ano)
		elif qtd_chars == 0:
			dados = self.m_relatorios.ConsolidadoPorMes()
		
		for dado in dados:
			self.list_relatorio.append([dado['mes'], dado['ano'], dado['total'], 'edit-find'])

		self.tree_relatorio.set_model(self.list_relatorio)

	def VisualizarDados(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			colunas = [{'coluna':'ano', 'valor':0},{'coluna':'mes', 'valor':0}]

			model, treeiter = treeview.get_selection().get_selected()
			mes = model[treeiter][0]
			colunas[1]['valor'] = mes

			model, treeiter = treeview.get_selection().get_selected()
			ano = model[treeiter][1]
			colunas[0]['valor'] = ano

			WinListaGames(colunas)