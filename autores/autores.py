import gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from models.models import *

class WinAdicionaAutor:
	def __init__(self):
		self.handlers = {
			"add_autor": self.AdicionaAutor
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/autores/win_add_autor.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_add_autor = self.builder.get_object("win_add_autor")
		self.win_add_autor.connect("destroy", Gtk.main_quit)
		self.win_add_autor.show_all()

		Gtk.main()

	def AdicionaAutor(self, widget):
		entry = self.builder.get_object('entry_autor')
		autor = entry.get_text()
		m_autores = AutoresModel()
		if not m_autores.TestaSeExisteAutor(autor):
			m_autores.Inserir(autor)
			entry.set_text('')
			self.Info('sucesso', 'Autor cadastrado com sucesso!')
		else:
			self.Info('erro', 'Autor já cadastrado!')

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()

class WinListaAutores:
	def __init__(self):
		self.handlers = {
			"busca_autor": self.BuscaAutor
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/autores/win_lista_autores.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_autores = AutoresModel()
		self.list_autores = Gtk.ListStore(int, str, str)
		self.tree_autores = self.builder.get_object('treeview_autores')
		self.ExibeLista()
		self.win_lista_autor = self.builder.get_object("win_lista_autores")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeLista(self):
		autores = self.m_autores.Listar()
		for autor in autores:
			self.list_autores.append([autor['id'], autor['autor'], 'document-edit'])

		self.tree_autores.set_model(self.list_autores)
		
		renderer_id = Gtk.CellRendererText()
		column_id = Gtk.TreeViewColumn("ID", renderer_id, text=0)
		self.tree_autores.append_column(column_id)
		
		renderer_autor = Gtk.CellRendererText()
		column_autor = Gtk.TreeViewColumn("Autor", renderer_autor, text=1)
		self.tree_autores.append_column(column_autor)

		renderer_editar = Gtk.CellRendererPixbuf()
		renderer_editar.set_property('xalign', 0.0)
		column_editar = Gtk.TreeViewColumn("Editar", renderer_editar, icon_name=2)
		self.tree_autores.append_column(column_editar)

		self.tree_autores.set_activate_on_single_click(True)
		self.tree_autores.connect("row_activated", self.EditarAutor)

	def EditarAutor(self, treeview, path, column):
		if column.get_title() == 'Editar':
			model, treeiter = treeview.get_selection().get_selected()
			id_autor = model[treeiter][0]
			win_edita_autor = WinEditaAutor(id_autor)
  
	def BuscaAutor(self, widget):
		entry = self.builder.get_object('entry_autor')
		autor = entry.get_text()
		qtd_chars = len(autor)
		
		if qtd_chars >= 3:
			self.list_autores.clear()
			autores = self.m_autores.GetAutorByNome(autor)

			for autor in autores:
				self.list_autores.append([autor['id'], autor['autor'], 'document-edit'])
			
			self.tree_autores.set_model(self.list_autores)
		elif qtd_chars == 0:
			self.list_autores.clear()
			autores = self.m_autores.Listar()

			for autor in autores:
				self.list_autores.append([autor['id'], autor['autor'], 'document-edit'])
			
			self.tree_autores.set_model(self.list_autores)

class WinEditaAutor:
	def __init__(self, id_autor):
		self.id_autor = id_autor
		self.handlers = {
			"salvar_autor": self.SalvaAutor
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/autores/win_edita_autor.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_add_autor = self.builder.get_object("win_edita_autor")
		self.win_add_autor.connect("destroy", Gtk.main_quit)
		self.win_add_autor.show_all()
		self.GetAutor()

		Gtk.main()

	def SalvaAutor(self, widget):
		entry = self.builder.get_object('entry_autor')
		autor_nome = entry.get_text()
		autor = {'autor':autor_nome, 'id':self.id_autor}
		m_autores = AutoresModel()
		if not m_autores.TestaSeExisteAutor(autor_nome):
			m_autores.Atualizar(autor)
			self.Info('sucesso', 'Autor atualizado com sucesso!')
		else:
			self.Info('erro', 'Autor já cadastrado!')

	def GetAutor(self):
		m_autores = AutoresModel()
		autor = m_autores.GetAutorById(self.id_autor)

		entry = self.builder.get_object('entry_autor')
		entry.set_text(autor['autor'])

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()