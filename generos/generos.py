import gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from models.models import *

class WinAdicionaGenero:
	def __init__(self):
		self.handlers = {
			"add_genero": self.AdicionaGenero
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/estudios/win_add_genero.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_add_genero = self.builder.get_object("win_add_genero")
		self.win_add_genero.connect("destroy", Gtk.main_quit)
		self.win_add_genero.show_all()

		Gtk.main()

	def AdicionaGenero(self, widget):
		entry = self.builder.get_object('entry_genero')
		genero = entry.get_text()
		m_generos = GenerosModel()
		if not m_generos.TestaSeExisteGenero(genero):
			m_generos.Inserir(genero)
			entry.set_text('')
			self.Info('sucesso', 'Gênero cadastrado com sucesso!')
		else:
			self.Info('erro', 'Gênero já cadastrado!')

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()

class WinListaGeneros:
	def __init__(self):
		self.handlers = {
			"busca_genero": self.BuscaGenero
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/estudios/win_lista_generos.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_generos = GenerosModel()
		self.list_generos = Gtk.ListStore(int, str, str)
		self.tree_generos = self.builder.get_object('treeview_generos')
		self.ExibeLista()
		self.win_lista_generos = self.builder.get_object("win_lista_generos")
		self.win_lista_generos.connect("destroy", Gtk.main_quit)
		self.win_lista_generos.show_all()

		Gtk.main()

	def ExibeLista(self):
		generos = self.m_generos.Listar()
		for genero in generos:
			self.list_generos.append([genero['id'], genero['genero'], 'document-edit'])

		self.tree_generos.set_model(self.list_generos)
		
		renderer_id = Gtk.CellRendererText()
		column_id = Gtk.TreeViewColumn("ID", renderer_id, text=0)
		self.tree_generos.append_column(column_id)
		
		renderer_editora = Gtk.CellRendererText()
		column_genero = Gtk.TreeViewColumn("Gênero", renderer_editora, text=1)
		self.tree_generos.append_column(column_genero)

		renderer_editar = Gtk.CellRendererPixbuf()
		renderer_editar.set_property('xalign', 0.0)
		column_editar = Gtk.TreeViewColumn("Editar", renderer_editar, icon_name=2)
		self.tree_generos.append_column(column_editar)

		self.tree_generos.set_activate_on_single_click(True)
		self.tree_generos.connect("row_activated", self.EditarGenero)

	def EditarGenero(self, treeview, path, column):
		if column.get_title() == 'Editar':
			model, treeiter = treeview.get_selection().get_selected()
			id_genero = model[treeiter][0]
			win_edita_genero = WinEditaGenero(id_genero)

	def BuscaGenero(self, widget):
		entry = self.builder.get_object('entry_genero')
		genero = entry.get_text()
		qtd_chars = len(genero)
		
		if qtd_chars >= 3:
			self.list_generos.clear()
			generos = self.m_generos.GetGenerosByNome(genero)

			for genero in generos:
				self.list_generos.append([genero['id'], genero['genero'], 'document-edit'])
			
			self.tree_generos.set_model(self.list_generos)
		elif qtd_chars == 0:
			self.list_generos.clear()
			generos = self.m_generos.Listar()

			for genero in generos:
				self.list_generos.append([genero['id'], genero['genero'], 'document-edit'])
			
			self.tree_generos.set_model(self.list_generos)

class WinEditaGenero:
	def __init__(self, id_genero):
		self.id_genero = id_genero
		self.handlers = {
			"salvar_genero": self.SalvaGenero
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/estudios/win_edita_genero.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_edita_genero = self.builder.get_object("win_edita_genero")
		self.win_edita_genero.connect("destroy", Gtk.main_quit)
		self.win_edita_genero.show_all()
		self.GetGenero()

		Gtk.main()

	def SalvaGenero(self, widget):
		entry = self.builder.get_object('entry_genero')
		genero_nome = entry.get_text()
		genero = {'genero':genero_nome, 'id_genero':self.id_genero}
		m_genero = GenerosModel()
		if not m_genero.TestaSeExisteGenero(genero_nome):
			m_genero.Atualizar(**genero)
			self.Info('sucesso', 'Gênero atualizado com sucesso!')
		else:
			self.Info('erro', 'Gênero já cadastrado!')

	def GetGenero(self):
		m_genero = GenerosModel()
		genero = m_genero.GetGeneroById(self.id_genero)

		entry = self.builder.get_object('entry_genero')
		entry.set_text(genero['genero'])

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()