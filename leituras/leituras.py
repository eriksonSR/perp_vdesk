import gi, json, requests, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from models.models import *
from apis.apis import *

class WinAdicionaLeitura:
	def __init__(self):
		self.handlers = {
			"add_leitura": self.AdicionaLeitura
		}
		self.assuntos_marcados = []
		self.autores_marcados = []
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/leituras/win_add_leitura.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_add_leitura = self.builder.get_object("win_add_leitura")
		self.win_add_leitura.connect("destroy", Gtk.main_quit)
		self.PreencheTiposLeitura()
		self.PreencheAutores()
		self.PreencheEditoras()
		self.PreencheAssuntos()
		self.PreencheFlagDigital()
		self.dir_json = './dados_apis/skoob/json/'
		self.win_add_leitura.show_all()

		Gtk.main()

	def AdicionaLeitura(self, widget):
		leitura = {
			'autores': [],
			'editoras': [],
			'assuntos': [],
			'tipo_midia': 0,
			'id_skoob': 0,
			'digital': 0,
			'favorito': 0,
			'dt_fim': '',
			'nota': 0,
			'pags': 0,
			'resenha': '',
			'sinopse': '',
			'titulo': '',
			'subtitulo': '',
			'ano': 0,
			'isbn': 0,
			'marcacoes': {},
			'capas': {'grande':'', 'media':'', 'pequena':'', 'micro':'', 'nano':''},
		}
		leitura['id_skoob'] = self.builder.get_object('entry_id_skoob').get_text()
		leitura['isbn'] = self.builder.get_object('entry_isbn').get_text()
		leitura['assuntos'] = self.assuntos_marcados
		leitura['autores'] = self.autores_marcados

		combo_tipo_midia = self.builder.get_object('combo_tipo_midia')
		leitura['tipo_midia'] = combo_tipo_midia.get_model()[combo_tipo_midia.get_active()][0]

		combo_editora = self.builder.get_object('combo_editora')
		leitura['editoras'].append(combo_editora.get_model()[combo_editora.get_active()][0])

		combo_digital = self.builder.get_object('combo_flag_digital')
		leitura['digital'] = combo_digital.get_model()[combo_digital.get_active()][0]

		api = SkoobApi(leitura['id_skoob'], 1)
		
		arq = r'{}{}.json'.format(self.dir_json, leitura['id_skoob'])
		with open(arq, 'r') as arq_j:
			j = json.load(arq_j)
			leitura['sinopse'] = j['response']['sinopse']
			leitura['pags'] = j['response']['paginas']
			leitura['ano'] = j['response']['ano']
			leitura['titulo'] = j['response']['titulo']
			leitura['dt_fim'] = j['response']['meu_livro']['dt_leitura']
			leitura['favorito'] = j['response']['meu_livro']['favorito']
			leitura['nota'] = j['response']['meu_livro']['ranking']

		capas = api.GetCapasUrls()
		leitura['capas']['grande'] = capas['capa_grande']
		leitura['capas']['media'] = capas['capa_media']
		leitura['capas']['pequena'] = capas['capa_pequena']
		leitura['capas']['micro'] = capas['capa_micro']
		leitura['capas']['nano'] = capas['capa_nano']

		m_leituras = LeiturasModel()
		m_leituras.Inserir(**leitura)
		self.BaixarCapas(capas, leitura['id_skoob'])

	def PreencheTiposLeitura(self):
		list_tipos = Gtk.ListStore(int, str)
		list_tipos.append([1, "Livro"])
		list_tipos.append([2, "HQ"])
		list_tipos.append([3, "Mangá"])
		combo = self.builder.get_object('combo_tipo_midia')
		combo.set_model(list_tipos)
		cell = Gtk.CellRendererText()
		combo.pack_start(cell, True)
		combo.add_attribute(cell, "text", 1)
		combo.set_active(0)

	def PreencheFlagDigital(self):
		list_tipos = Gtk.ListStore(int, str)
		list_tipos.append([1, 'Sim'])
		list_tipos.append([0, 'Não'])
		combo = self.builder.get_object('combo_flag_digital')
		combo.set_model(list_tipos)
		cell = Gtk.CellRendererText()
		combo.pack_start(cell, True)
		combo.add_attribute(cell, "text", 1)
		combo.set_active(1)

	def PreencheAutores(self):
		m_autores = AutoresModel()
		autores = m_autores.Listar()
		listbox = Gtk.ListBox()

		for autor in autores:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(autor['autor'])
			ckbutton.connect("toggled", self.OnAutorToggled, autor['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_autores')
		scroll.add(listbox)

	def PreencheEditoras(self):
		list_editoras = Gtk.ListStore(int, str)
		m_editoras = EditorasModel()
		editoras = m_editoras.Listar()
		
		for editora in editoras:
			list_editoras.append([editora['id'], editora['editora']])
		
		combo = self.builder.get_object('combo_editora')
		combo.set_model(list_editoras)
		cell = Gtk.CellRendererText()
		combo.pack_start(cell, True)
		combo.add_attribute(cell, "text", 1)
		combo.set_active(1)

	def PreencheAssuntos(self):
		m_assuntos = AssuntosModel()
		assuntos = m_assuntos.Listar()
		listbox = Gtk.ListBox()
		
		for assunto in assuntos:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(assunto['assunto'])
			ckbutton.connect("toggled", self.OnAssuntoToggled, assunto['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_assuntos')
		scroll.add(listbox)

	def OnAssuntoToggled(self, button, id_assunto):
		if button.get_active():
			if id_assunto not in self.assuntos_marcados:
				self.assuntos_marcados.append(id_assunto)
		else:
			self.assuntos_marcados.remove(id_assunto)

	def OnAutorToggled(self, button, id_autor):
		if button.get_active():
			if id_autor not in self.autores_marcados:
				self.autores_marcados.append(id_autor)
		else:
			self.autores_marcados.remove(id_autor)

	def BaixarCapas(self, capas, id_skoob):
		diretorio = r'./imgs/leituras/{}'.format(id_skoob)
			
		if not os.path.exists(diretorio):
			os.makedirs(diretorio)

		arq_grande = r'./imgs/leituras/{}/capa_grande.jpg'.format(id_skoob)
		arq_media = r'./imgs/leituras/{}/capa_media.jpg'.format(id_skoob)
		arq_pequena = r'./imgs/leituras/{}/capa_pequena.jpg'.format(id_skoob)
		arq_micro = r'./imgs/leituras/{}/capa_micro.jpg'.format(id_skoob)
		arq_nano = r'./imgs/leituras/{}/capa_nano.jpg'.format(id_skoob)

		img_grande = requests.get(capas['capa_grande']).content
		img_media = requests.get(capas['capa_media']).content
		img_pequena = requests.get(capas['capa_pequena']).content
		img_micro = requests.get(capas['capa_micro']).content
		img_nano = requests.get(capas['capa_nano']).content
			
		with open(arq_grande, 'wb') as handler:
			handler.write(img_grande)
			
		with open(arq_media, 'wb') as handler:
		    handler.write(img_media)
			
		with open(arq_pequena, 'wb') as handler:
		    handler.write(img_pequena)
			
		with open(arq_micro, 'wb') as handler:
		    handler.write(img_micro)
		
		with open(arq_nano, 'wb') as handler:
		    handler.write(img_nano)

class WinListaLeituras:
	def __init__(self, filtros):
		self.filtros = filtros
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/leituras/win_lista_leituras.glade'))
		self.m_relatorios = LeiturasRelatoriosModel()
		self.list_leituras = Gtk.ListStore(int, str, str, str, str, str, str)
		self.tree_leituras = self.builder.get_object('treeview_lista_leituras')
		self.ExibeDados()
		self.win_lista_leituras = self.builder.get_object("win_lista_leituras")
		self.win_lista_leituras.connect("destroy", Gtk.main_quit)
		self.win_lista_leituras.show_all()

		Gtk.main()

	def ExibeDados(self):
		leituras = self.m_relatorios.ListagemGeral(self.filtros)
		for leitura in leituras:
			self.list_leituras.append([leitura['id'], leitura['titulo'], leitura['editora'], leitura['tipo'], leitura['formato'], leitura['dt'], 'edit-find'])

		self.tree_leituras.set_model(self.list_leituras)
		
		renderer_titulo = Gtk.CellRendererText()
		column_titulo = Gtk.TreeViewColumn("Título", renderer_titulo, text=1)
		self.tree_leituras.append_column(column_titulo)
		
		renderer_editora = Gtk.CellRendererText()
		column_editora = Gtk.TreeViewColumn("Editora", renderer_editora, text=2)
		self.tree_leituras.append_column(column_editora)
		
		renderer_tipo = Gtk.CellRendererText()
		column_tipo = Gtk.TreeViewColumn("Tipo", renderer_tipo, text=3)
		self.tree_leituras.append_column(column_tipo)
		
		renderer_formato = Gtk.CellRendererText()
		column_formato = Gtk.TreeViewColumn("Formato", renderer_formato, text=4)
		self.tree_leituras.append_column(column_formato)
		
		renderer_data = Gtk.CellRendererText()
		column_data = Gtk.TreeViewColumn("Data", renderer_data, text=5)
		self.tree_leituras.append_column(column_data)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=6)
		self.tree_leituras.append_column(column_visualizar)

		self.tree_leituras.set_activate_on_single_click(True)
		self.tree_leituras.connect("row_activated", self.VisualizarFichaLeitura)

	def VisualizarFichaLeitura(self, treeview, path, column):
		if column.get_title() == 'Visualizar':
			model, treeiter = treeview.get_selection().get_selected()
			id_leitura = model[treeiter][0]

			WinExibeLeitura(id_leitura)

class WinExibeLeitura:
	def __init__(self, id_leitura):
		self.id_leitura = id_leitura
		self.m_leituras = LeiturasModel()
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/leituras/win_exibe_leitura.glade'))
		self.win_exibe_leitura = self.builder.get_object("win_exibe_leitura")
		self.win_exibe_leitura.connect("destroy", Gtk.main_quit)
		self.PreencheDados()
		self.win_exibe_leitura.show_all()
		Gtk.main()

	def PreencheDados(self):
		img = self.builder.get_object('image_capa')
		scrolled_sinopse = self.builder.get_object('scrolled_sinopse')
		scrolled_resenha = self.builder.get_object('scrolled_resenha')
		label_sinopse = Gtk.Label()
		label_resenha = Gtk.Label()
		leitura = self.m_leituras.GetLeituraById(self.id_leitura)
		autores = self.m_leituras.GetAutoresDaLeitura(self.id_leitura)
		autores_concat = "<b>Autores:</b> "
		for autor in autores:
			autores_concat = autores_concat + autor['autor'] + ", "
		autores_concat = autores_concat[0:-2]

		assuntos = self.m_leituras.GetAssuntosDaLeitura(self.id_leitura)
		assuntos_concat = "<b>Assuntos:</b> "
		for assunto in assuntos:
			assuntos_concat = assuntos_concat + assunto['assunto'] + ", "
		assuntos_concat = assuntos_concat[0:-2]
		img.set_from_file(os.path.realpath('./imgs/leituras/{}/capa_media.jpg'.format(leitura['id_skoob'])))
		label_sinopse.set_line_wrap(True)
		label_resenha.set_line_wrap(True)
		label_sinopse.set_text(leitura['sinopse'])
		label_resenha.set_text(leitura['resenha'])
		self.builder.get_object('label_titulo').set_markup("<b>Título:</b> {}".format(leitura['titulo']))
		self.builder.get_object('label_ano').set_markup("<b>Ano:</b> {}".format(leitura['ano']))
		self.builder.get_object('label_nota').set_markup("<b>Nota:</b> {}".format(leitura['nota']))
		self.builder.get_object('label_isbn').set_markup("<b>ISBN:</b> {}".format(leitura['isbn']))
		self.builder.get_object('label_num_pags').set_markup("<b>Páginas:</b> {}".format(leitura['paginas']))
		self.builder.get_object('label_tipo_midia').set_markup("<b>Tipo de mídia:</b> {}".format(leitura['tipo']))
		self.builder.get_object('label_editora').set_markup("<b>Editora:</b> {}".format(leitura['editora']))
		self.builder.get_object('label_formato').set_markup("<b>Formato:</b> {}".format(leitura['formato']))
		self.builder.get_object('label_dt_finalizacao').set_markup("<b>Data finalização:</b> {}".format(leitura['dt_finalizacao']))
		self.builder.get_object('label_autores').set_markup(autores_concat)
		self.builder.get_object('label_assuntos').set_markup(assuntos_concat)
		scrolled_sinopse.add_with_viewport(label_sinopse)
		scrolled_resenha.add_with_viewport(label_resenha)