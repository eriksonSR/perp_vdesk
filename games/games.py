import gi, json, requests, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf
from models.models import *
from apis.apis import *

class WinAdicionaGame:
	def __init__(self):
		self.handlers = {
			"add_game": self.AdicionaGame
		}
		self.desenvolvedoras_marcadas = []
		self.publicadoras_marcadas = []
		self.generos_marcados = []
		self.url_imgs = {'boxes':[], 'screens':[], 'arts':[]}
		self.header = {'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/68.0.3440.75 Chrome/68.0.3440.75 Safari/537.36'}
		self.api_igdb = ''
		self.api_gdb = ''
		self.id_igdb = 0
		self.id_gdb = 0
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/games/win_add_game.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_add_game = self.builder.get_object("win_add_game")
		self.win_add_game.connect("destroy", Gtk.main_quit)
		self.PreencheDesenvolvedoras()
		self.PreenchePublicadoras()
		self.PreencheFlagDigital()
		self.PreencheFlagDlc()
		self.PreencheConsoles()
		self.PreencheGeneros()
		self.win_add_game.show_all()

		Gtk.main()

	def AdicionaGame(self, widget):
		game = {
			'id_igdb':self.builder.get_object('entry_id_igdb').get_text(),
			'id_gdb':self.builder.get_object('entry_id_gdb').get_text(),
			'id_console':0,
			'sinopse':'',
			'digital':0,
			'dlc':0,
			'videos':{},
			'titulo':self.builder.get_object('entry_titulo').get_text(),
			'dt_finalizacao':self.builder.get_object('entry_dt_finalizacao').get_text(),
			'imagens':{},
			'publicadoras':self.publicadoras_marcadas,
			'desenvolvedoras':self.desenvolvedoras_marcadas,
			'generos':self.generos_marcados
		}
		self.id_igdb = game['id_igdb']
		self.gdb = game['id_gdb']

		combo_console = self.builder.get_object('combo_console')
		game['id_console'] = combo_console.get_model()[combo_console.get_active()][0]

		combo_flag_dlc = self.builder.get_object('combo_flag_dlc')
		game['dlc'] = combo_flag_dlc.get_model()[combo_flag_dlc.get_active()][0]

		combo_flag_digital = self.builder.get_object('combo_flag_digital')
		game['digital'] = combo_flag_digital.get_model()[combo_flag_digital.get_active()][0]

		self.api_igdb = IgdbApi(game['id_igdb'])
		self.api_gdb = GamesDataBaseApi(game['id_gdb'])

		self.api_igdb.GetJson()
		self.api_gdb.GetJson()

		self.GetImages()
		game['imagens'] = self.url_imgs;
		game['videos'] = self.api_igdb.GetVideosUrls()
		game['sinopse'] = self.api_igdb.GetSummary()

		m_games = GamesModel()
		m_games.Inserir(**game)

	def PreencheFlagDigital(self):
		list_tipos = Gtk.ListStore(int, str)
		list_tipos.append([1, 'Sim'])
		list_tipos.append([0, 'Não'])
		combo = self.builder.get_object('combo_flag_digital')
		combo.set_model(list_tipos)
		cell = Gtk.CellRendererText()
		combo.pack_start(cell, True)
		combo.add_attribute(cell, "text", 1)
		combo.set_active(1)

	def PreencheFlagDlc(self):
		list_tipos = Gtk.ListStore(int, str)
		list_tipos.append([1, 'Sim'])
		list_tipos.append([0, 'Não'])
		combo = self.builder.get_object('combo_flag_dlc')
		combo.set_model(list_tipos)
		cell = Gtk.CellRendererText()
		combo.pack_start(cell, True)
		combo.add_attribute(cell, "text", 1)
		combo.set_active(1)

	def PreencheDesenvolvedoras(self):
		m_studios = StudiosModel()
		studios = m_studios.Listar()
		listbox = Gtk.ListBox()
		
		for studio in studios:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(studio['studio'])
			ckbutton.connect("toggled", self.OnDesenvolvedoraToggled, studio['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_desenvolvedoras')
		scroll.add(listbox)

	def PreenchePublicadoras(self):
		m_studios = StudiosModel()
		studios = m_studios.Listar()
		listbox = Gtk.ListBox()
		
		for studio in studios:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(studio['studio'])
			ckbutton.connect("toggled", self.OnPublicadoraToggled, studio['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_publicadoras')
		scroll.add(listbox)

	def PreencheGeneros(self):
		m_generos = GenerosModel()
		generos = m_generos.Listar()
		listbox = Gtk.ListBox()
		
		for genero in generos:
			row = Gtk.ListBoxRow()
			ckbutton = Gtk.CheckButton.new_with_label(genero['genero'])
			ckbutton.connect("toggled", self.OnGeneroToggled, genero['id'])
			row.add(ckbutton)
			listbox.add(row)

		scroll = self.builder.get_object('scroll_generos')
		scroll.add(listbox)

	def PreencheConsoles(self):
		list_consoles = Gtk.ListStore(int, str)
		m_consoles = ConsolesModel()
		consoles = m_consoles.Listar()
		
		for console in consoles:
			list_consoles.append([console['id'], console['console']])
		
		combo = self.builder.get_object('combo_console')
		combo.set_model(list_consoles)
		cell = Gtk.CellRendererText()
		combo.pack_start(cell, True)
		combo.add_attribute(cell, "text", 1)
		combo.set_active(1)

	def OnDesenvolvedoraToggled(self, button, id_studio):
		if button.get_active():
			if id_studio not in self.desenvolvedoras_marcadas:
				self.desenvolvedoras_marcadas.append(id_studio)
		else:
			self.desenvolvedoras_marcadas.remove(id_studio)

	def OnPublicadoraToggled(self, button, id_studio):
		if button.get_active():
			if id_studio not in self.publicadoras_marcadas:
				self.publicadoras_marcadas.append(id_studio)
		else:
			self.publicadoras_marcadas.remove(id_studio)

	def OnGeneroToggled(self, button, id_genero):
		if button.get_active():
			if id_genero not in self.generos_marcados:
				self.generos_marcados.append(id_genero)
		else:
			self.generos_marcados.remove(id_genero)

	def GetImages(self):
		box_igdb = self.api_igdb.GetBoxArtsUrls()
		screens_igdb = self.api_igdb.GetScreenshotsUrls()
		arts_igdb = self.api_igdb.GetArtWorksUrls()

		box_gdb = self.api_gdb.GetBoxArtsUrls()
		screens_gdb = self.api_gdb.GetScreenshotsUrls()
		arts_gdb = self.api_gdb.GetArtWorksUrls()
		
		for i in box_igdb['originais']:
			self.url_imgs['boxes'].append(i)
		for i in screens_igdb['originais']:
			self.url_imgs['screens'].append(i)
		for i in arts_igdb['originais']:
			self.url_imgs['arts'].append(i)
		
		for i in box_gdb['originais']:
			self.url_imgs['boxes'].append(i)
		for i in screens_gdb['originais']:
			self.url_imgs['screens'].append(i)
		for i in arts_gdb['originais']:
			self.url_imgs['arts'].append(i)

		print('Baixando capas...')
		cont = 0
		for i in self.url_imgs['boxes']:
			cont = cont + 1
			url_parts = i.split('/')
			dt_img = url_parts[-1].split('.')
			img_ext = dt_img[1]
			print('URL: {}'.format(i))
			print('CAMINHO: {}'.format('./imgs/games/boxarts/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext)))
			with open(r'./imgs/games/boxarts/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext), 'wb') as handle:
				response = requests.get(i, headers=self.header, stream=True)
				print('Resposta: {}'.format(response.status_code))
				handle.write(response.content)

		print('Baixando screenshots...')
		cont = 0
		for i in self.url_imgs['screens']:
			cont = cont + 1
			url_parts = i.split('/')
			dt_img = url_parts[-1].split('.')
			img_ext = dt_img[1]
			print('URL: {}'.format(i))
			print('CAMINHO: {}'.format('./imgs/games/screenshots/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext)))
			with open(r'./imgs/games/screenshots/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext), 'wb') as handle:
				response = requests.get(i, headers=self.header, stream=True)
				print('Resposta: {}'.format(response.status_code))
				handle.write(response.content)

		print('Baixando fanarts...')
		cont = 0
		for i in self.url_imgs['arts']:
			cont = cont + 1
			url_parts = i.split('/')
			dt_img = url_parts[-1].split('.')
			img_ext = dt_img[1]
			print('URL: {}'.format(i))
			print('CAMINHO: {}'.format('./imgs/games/fanarts/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext)))
			with open(r'./imgs/games/fanarts/originais/{}-{}.{}'.format(self.id_igdb, cont, img_ext), 'wb') as handle:
				response = requests.get(i, headers=self.header, stream=True)
				print('Resposta: {}'.format(response.status_code))
				handle.write(response.content)

class WinListaGames:
	def __init__(self, filtros):
		self.filtros = filtros
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/games/win_lista_games.glade'))
		self.m_relatorios = GamesRelatoriosModel()
		self.list_games = Gtk.ListStore(int, str, str, str, str, str, str, str)
		self.tree_games = self.builder.get_object('treeview_lista_games')
		self.ExibeDados()
		self.win_lista_games = self.builder.get_object("win_lista_games")
		self.win_lista_games.connect("destroy", Gtk.main_quit)
		self.win_lista_games.show_all()

		Gtk.main()

	def ExibeDados(self):
		games = self.m_relatorios.ListagemGeral(self.filtros)
		for game in games:
			self.list_games.append([game['id'], game['titulo'], game['tipo'], game['plataforma'], game['console'], game['portatil'], game['dt'], 'edit-find'])

		self.tree_games.set_model(self.list_games)
		
		renderer_titulo = Gtk.CellRendererText()
		column_titulo = Gtk.TreeViewColumn("Título", renderer_titulo, text=1)
		self.tree_games.append_column(column_titulo)
		
		renderer_tipo = Gtk.CellRendererText()
		column_tipo = Gtk.TreeViewColumn("Tipo", renderer_tipo, text=2)
		self.tree_games.append_column(column_tipo)
		
		renderer_console = Gtk.CellRendererText()
		column_console = Gtk.TreeViewColumn("Plataforma", renderer_console, text=3)
		self.tree_games.append_column(column_console)
		
		renderer_console = Gtk.CellRendererText()
		column_console = Gtk.TreeViewColumn("Console", renderer_console, text=4)
		self.tree_games.append_column(column_console)
		
		renderer_portatil = Gtk.CellRendererText()
		column_portatil = Gtk.TreeViewColumn("Portátil", renderer_portatil, text=5)
		self.tree_games.append_column(column_portatil)
		
		renderer_data = Gtk.CellRendererText()
		column_data = Gtk.TreeViewColumn("Data", renderer_data, text=6)
		self.tree_games.append_column(column_data)
		self.tree_games.append_column(column_data)

		renderer_visualizar = Gtk.CellRendererPixbuf()
		renderer_visualizar.set_property('xalign', 0.0)
		column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=7)
		self.tree_games.append_column(column_visualizar)

		self.tree_games.set_activate_on_single_click(True)
		self.tree_games.connect("row_activated", self.VisualizarFichaGame)

	def VisualizarFichaGame(self, treeview, path, column):
		if column.get_title() == 'Visualizar':

			model, treeiter = treeview.get_selection().get_selected()
			id_game = model[treeiter][0]
			WinExibeGame(id_game)

class WinExibeGame:
	def __init__(self, id_game):
		self.id_game = id_game
		self.m_games = GamesModel()
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/games/win_exibe_game.glade'))
		self.win_exibe_game = self.builder.get_object("win_exibe_game")
		self.win_exibe_game.connect("destroy", Gtk.main_quit)
		self.PreencheDados()
		self.win_exibe_game.show_all()
		Gtk.main()

	def PreencheDados(self):
		img = self.builder.get_object('image_capa')
		scrolled_sinopse = self.builder.get_object('scrolled_sinopse')
		label_sinopse = Gtk.Label()
		game = self.m_games.GetFichaGame(self.id_game)
		generos = self.m_games.GetGenerosGame(self.id_game)
		generos_concat = "<b>Gêneros:</b> "
		for genero in generos:
			generos_concat = generos_concat + genero['genero'] + ", "
		generos_concat = generos_concat[0:-2]

		desenvolvedoras = self.m_games.GetDesenvolvedorasGame(self.id_game)
		desenvolvedoras_concat = "<b>Desenvolvedoras:</b> "
		for desenvolvedora in desenvolvedoras:
			desenvolvedoras_concat = desenvolvedoras_concat + desenvolvedora['studio'] + ", "
		desenvolvedoras_concat = desenvolvedoras_concat[0:-2]
		img_path = os.path.realpath('./imgs/games/boxarts/originais/{}-1.jpg'.format(game['id_igdb']))
		pixbuf_img = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename=img_path, width=400, height=500, preserve_aspect_ratio=True)

		img.set_from_pixbuf(pixbuf_img)
		label_sinopse.set_line_wrap(True)
		label_sinopse.set_text(game['sinopse'])
		self.builder.get_object('label_titulo').set_markup("<b>Título:</b> {}".format(game['titulo']))
		self.builder.get_object('label_console').set_markup("<b>Console:</b> {}".format(game['console']))
		self.builder.get_object('label_digital').set_markup("<b>Digital:</b> {}".format(game['digital']))
		self.builder.get_object('label_dlc').set_markup("<b>DLC:</b> {}".format(game['dlc']))
		self.builder.get_object('label_publicadora').set_markup("<b>Publicadora:</b> {}".format(game['studio']))
		self.builder.get_object('label_dt_finalizacao').set_markup("<b>Data finalização:</b> {}".format(game['dt']))
		self.builder.get_object('label_desenvolvedoras').set_markup(desenvolvedoras_concat)
		self.builder.get_object('label_generos').set_markup(generos_concat)
		scrolled_sinopse.add_with_viewport(label_sinopse)