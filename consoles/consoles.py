import gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from models.models import *

class WinListaConsoles:
	def __init__(self):
		self.handlers = {
			"busca_console": self.BuscaConsole
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/consoles/win_lista_consoles.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_consoles = ConsolesModel()
		self.m_plataformas = PlataformasModel()
		self.list_consoles = Gtk.ListStore(int, str, str, str)
		self.tree_consoles = self.builder.get_object('treeview_consoles')
		self.ExibeLista()
		self.win_lista_consoles = self.builder.get_object("win_lista_consoles")
		self.win_lista_consoles.connect("destroy", Gtk.main_quit)
		self.win_lista_consoles.show_all()

		Gtk.main()

	def ExibeLista(self):
		consoles = self.m_consoles.Listar()
		for console in consoles:
			self.list_consoles.append([console['id'], console['plataforma'], console['console'], 'document-edit'])

		self.tree_consoles.set_model(self.list_consoles)
		
		renderer_id = Gtk.CellRendererText()
		column_id = Gtk.TreeViewColumn("ID", renderer_id, text=0)
		self.tree_consoles.append_column(column_id)
		
		renderer_plataforma = Gtk.CellRendererText()
		column_plataforma = Gtk.TreeViewColumn("Plataforma", renderer_plataforma, text=1)
		self.tree_consoles.append_column(column_plataforma)
		
		renderer_console = Gtk.CellRendererText()
		column_console = Gtk.TreeViewColumn("Console", renderer_console, text=2)
		self.tree_consoles.append_column(column_console)

		renderer_editar = Gtk.CellRendererPixbuf()
		renderer_editar.set_property('xalign', 0.0)
		column_editar = Gtk.TreeViewColumn("Editar", renderer_editar, icon_name=3)
		self.tree_consoles.append_column(column_editar)

		self.tree_consoles.set_activate_on_single_click(True)
		self.tree_consoles.connect("row_activated", self.EditarConsole)

	def EditarConsole(self, treeview, path, column):
		if column.get_title() == 'Editar':
			model, treeiter = treeview.get_selection().get_selected()
			id_console = model[treeiter][0]
			win_edita_console = WinEditaConsole(id_console)

	def BuscaConsole(self, widget):
		entry = self.builder.get_object('entry_console')
		console = entry.get_text()
		qtd_chars = len(console)
		
		if qtd_chars >= 3:
			self.list_consoles.clear()
			consoles = self.m_consoles.GetConsolesByNome(console)

			for console in consoles:
				self.list_consoles.append([console['id'], console['plataforma'], console['console'], 'document-edit'])
			
			self.tree_estudios.set_model(self.list_consoles)
		elif qtd_chars == 0:
			self.list_consoles.clear()
			consoles = self.m_consoles.Listar()

			for console in consoles:
				self.list_consoles.append([console['id'], console['plataforma'], console['console'], 'document-edit'])
			
			self.tree_estudios.set_model(self.list_consoles)

class WinEditaConsole:
	def __init__(self, id_console):
		self.id_console = id_console
		self.handlers = {
			"salvar_console": self.SalvaConsole
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/consoles/win_edita_console.glade'))
		self.builder.connect_signals(self.handlers)
		self.PreenchePlataformas()
		self.win_edita_console = self.builder.get_object("win_edita_console")
		self.win_edita_console.connect("destroy", Gtk.main_quit)
		self.win_edita_console.show_all()
		self.GetConsole()

		Gtk.main()

	def SalvaConsole(self, widget):
		entry = self.builder.get_object('entry_console')
		console_nome = entry.get_text()
		combo_plataforma = self.builder.get_object('combo_plataforma')
		id_plataforma = combo_plataforma.get_model()[combo_plataforma.get_active()][0]

		console = {'console':console_nome, 'id_console':self.id_console, 'id_plataforma':id_plataforma}
		
		m_console = ConsolesModel()
		if not m_console.TestaSeExisteConsole(console['console'], console['id_plataforma']):
			m_console.Atualizar(**console)
			entry.set_text('')
			self.Info('sucesso', 'Console atualizado com sucesso!')
		else:
			self.Info('erro', 'Console já cadastrado!')

	def GetConsole(self):
		m_console = ConsolesModel()
		console = m_console.GetConsoleById(self.id_console)
		entry = self.builder.get_object('entry_console')
		entry.set_text(console['console'])

		combo_plataforma = self.builder.get_object('combo_plataforma')
		model_plataformas = combo_plataforma.get_model()

		cont = 0
		for plataforma in model_plataformas:
			if console['id_plataforma'] == plataforma[0]:
				break
			cont = cont + 1

		combo_plataforma.set_active(cont)

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()

	def PreenchePlataformas(self):
		m_plataformas = PlataformasModel()
		plataformas = m_plataformas.Listar()
		list_plataformas = Gtk.ListStore(int, str)
		
		for plataforma in plataformas:
			list_plataformas.append([plataforma['id'], plataforma['plataforma']])
		
		combo = self.builder.get_object('combo_plataforma')
		combo.set_model(list_plataformas)
		cell = Gtk.CellRendererText()
		combo.pack_start(cell, True)
		combo.add_attribute(cell, "text", 1)
		combo.set_active(1)

class WinAdicionaConsole:
	def __init__(self):
		self.handlers = {
			"add_console": self.AdicionaConsole
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/consoles/win_add_console.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_add_console = self.builder.get_object("win_add_console")
		self.win_add_console.connect("destroy", Gtk.main_quit)
		self.win_add_console.show_all()
		self.PreenchePlataformas()
		Gtk.main()

	def AdicionaConsole(self, widget):
		m_console = ConsolesModel()
		entry = self.builder.get_object('entry_console')
		console = {'console': entry.get_text(), 'id_plataforma':0}

		combo_plataforma = self.builder.get_object('combo_plataforma')
		console['id_plataforma'] = combo_plataforma.get_model()[combo_plataforma.get_active()][0]

		if not m_console.TestaSeExisteConsole(console['console']):
			m_console.Inserir(**console)
			entry.set_text('')
			self.Info('sucesso', 'Console cadastrado com sucesso!')
		else:
			self.Info('erro', 'Console já cadastrado!')

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()

	def PreenchePlataformas(self):
		m_plataformas = PlataformasModel()
		plataformas = m_plataformas.Listar()
		list_plataformas = Gtk.ListStore(int, str)
		
		for plataforma in plataformas:
			list_plataformas.append([plataforma['id'], plataforma['plataforma']])
		
		combo = self.builder.get_object('combo_plataforma')
		combo.set_model(list_plataformas)
		cell = Gtk.CellRendererText()
		combo.pack_start(cell, True)
		combo.add_attribute(cell, "text", 1)
		combo.set_active(1)