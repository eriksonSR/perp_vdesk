import gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from models.models import *
from autores.autores import *
from editoras.editoras import *
from assuntos.assuntos import *
from leituras.leituras import *
from estudios.estudios import *
from consoles.consoles import *
from generos.generos import *
from games.games import *
from relatorios.relatorios import *
from pprint import pprint
import time
import csv
import subprocess
import sys

class Handler:
    def __init__(self, win_principal):
        self.win_principal = win_principal

    def open_list_autores(self, *args):
        autor = WinListaAutores()

    def open_form_cad_autor(self, *args):
    	autor = WinAdicionaAutor()
    
    def open_list_editoras(self, *args):
        editora = WinListaEditoras()

    def open_form_cad_editora(self, *args):
    	editora = WinAdicionaEditora()
    
    def open_list_assuntos(self, *args):
        assunto = WinListaAssuntos()

    def open_form_cad_assunto(self, *args):
    	assunto = WinAdicionaAssunto()

    def open_form_cad_leitura(self, *args):
    	leitura = WinAdicionaLeitura()
    
    def open_list_estudios(self, *args):
        estudio = WinListaEstudios()

    def open_form_cad_estudio(self, *args):
    	estudio = WinAdicionaEstudio()
    
    def open_list_consoles(self, *args):
        console = WinListaConsoles()

    def open_form_cad_console(self, *args):
    	console = WinAdicionaConsole()

    def open_form_cad_genero(self, *args):
    	genero = WinAdicionaGenero()
    
    def open_list_generos(self, *args):
        generos = WinListaGeneros()
    
    def open_form_cad_game(self, *args):
        games = WinAdicionaGame()
    
    def open_rel_dlc_x_jogo_base(self, *args):
        rel = WinRelDlcXjogoBase()
    
    def open_rel_consolidado_por_ano(self, *args):
        rel = WinRelConsolidadoPorAno()
    
    def open_rel_consolidado_por_genero(self, *args):
        rel = WinRelConsolidadoPorGenero()
    
    def open_rel_consolidado_por_genero_ao_ano(self, *args):
        rel = WinRelConsolidadoPorGeneroAoAno()
    
    def open_rel_consolidado_por_desenvolvedora(self, *args):
        rel = WinRelConsolidadoPorDesenvolvedora()
    
    def open_rel_consolidado_por_publicadora(self, *args):
        rel = WinRelConsolidadoPorPublicadora()
    
    def open_rel_consolidado_por_console_ao_ano(self, *args):
        rel = WinRelConsolidadoPorConsoleAoAno()
    
    def open_rel_consolidado_por_plataforma_ao_ano(self, *args):
        rel = WinRelConsolidadoPorPlataformaAoAno()
    
    def open_rel_consolidado_portatil_x_mesa(self, *args):
        rel = WinRelConsolidadoPortatilXmesa()
    
    def open_rel_leituras_consolidado_por_ano(self, *args):
        rel = WinRelLeiturasConsolidadoPorAno()
    
    def open_rel_leituras_consolidado_por_assunto_ao_ano(self, *args):
        rel = WinRelLeiturasConsolidadoPorAssuntoAoAno()
    
    def open_rel_leituras_consolidado_por_autor(self, *args):
        rel = WinRelLeiturasConsolidadoPorAutor()
    
    def open_rel_leituras_consolidado_por_editora(self, *args):
        rel = WinRelLeiturasConsolidadoPorEditora()
    
    def open_rel_leituras_consolidado_por_formato_ao_ano(self, *args):
        rel = WinRelLeiturasConsolidadoPorFormatoAoAno()
    
    def open_rel_leituras_consolidado_por_tipo_midia_ao_ano(self, *args):
        rel = WinRelLeiturasConsolidadoPorTipoMidiaAoAno()

    def buscar(self, * args):
        self.win_principal.Buscar()
    
    def open_rel_leituras_consolidado_por_mes(self, *args):
        rel = WinRelLeiturasConsolidadoPorMes()
    
    def open_rel_games_consolidado_por_mes(self, *args):
        rel = WinRelGamesConsolidadoPorMes()

    def gerar_csv(self, * args):
        self.win_principal.GeraCsv()

class WinPrincipal:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file(os.path.realpath('./janelas/win_principal.glade'))
        self.builder.connect_signals(Handler(self))
        self.m_rel_games = GamesRelatoriosModel()
        self.m_rel_leituras = LeiturasRelatoriosModel()
        self.list_games = Gtk.ListStore(int, str, str, str, str)
        self.list_leituras = Gtk.ListStore(int, str, str, str, str, str)
        self.tree_games = self.builder.get_object("treeview_games")
        self.tree_leituras = self.builder.get_object("treeview_leituras")
        self.PreencheGames()
        self.PreencheLeituras()
        self.win_principal = self.builder.get_object("win_principal")
        self.win_principal.connect("destroy", Gtk.main_quit)
        self.win_principal.show_all()

        Gtk.main()

    def PreencheGames(self):
        games = self.m_rel_games.ListagemGeral()

        for game in games:
            self.list_games.append([game['id'], game['titulo'], game['console'], game['dt'], 'edit-find'])

        self.tree_games.set_model(self.list_games)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Titulo", renderer, text=1)
        self.tree_games.append_column(column)
        column = Gtk.TreeViewColumn("Console", renderer, text=2)
        self.tree_games.append_column(column)
        column = Gtk.TreeViewColumn("Data", renderer, text=3)
        self.tree_games.append_column(column)

        renderer_visualizar = Gtk.CellRendererPixbuf()
        renderer_visualizar.set_property('xalign', 0.0)
        column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=4)
        self.tree_games.append_column(column_visualizar)

        self.tree_games.set_activate_on_single_click(True)
        self.tree_games.connect("row_activated", self.VisualizarFichaGame)

    def PreencheLeituras(self):
        leituras = self.m_rel_leituras.ListagemGeral()
        for leitura in leituras:
            self.list_leituras.append([leitura['id'], leitura['titulo'], leitura['editora'], leitura['tipo'], leitura['dt'], 'edit-find'])

        self.tree_leituras.set_model(self.list_leituras)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Titulo", renderer, text=1)
        self.tree_leituras.append_column(column)
        column = Gtk.TreeViewColumn("Editora", renderer, text=2)
        self.tree_leituras.append_column(column)
        column = Gtk.TreeViewColumn("Tipo", renderer, text=3)
        self.tree_leituras.append_column(column)
        column = Gtk.TreeViewColumn("Data", renderer, text=4)
        self.tree_leituras.append_column(column)

        renderer_visualizar = Gtk.CellRendererPixbuf()
        renderer_visualizar.set_property('xalign', 0.0)
        column_visualizar = Gtk.TreeViewColumn("Visualizar", renderer_visualizar, icon_name=5)
        self.tree_leituras.append_column(column_visualizar)

        self.tree_leituras.set_activate_on_single_click(True)
        self.tree_leituras.connect("row_activated", self.VisualizarFichaLeitura)

    def VisualizarFichaLeitura(self, treeview, path, column):
        if column.get_title() == 'Visualizar':

            model, treeiter = treeview.get_selection().get_selected()
            id_leitura = model[treeiter][0]
            WinExibeLeitura(id_leitura)

    def VisualizarFichaGame(self, treeview, path, column):
        if column.get_title() == 'Visualizar':

            model, treeiter = treeview.get_selection().get_selected()
            id_game = model[treeiter][0]
            WinExibeGame(id_game)

    def Buscar(self):
        notebook = self.builder.get_object("notebook_leituras_games")
        aba = notebook.get_current_page()
        if aba == 0:
            colunas = [{'coluna':'titulo', 'valor':self.builder.get_object('entry_busca').get_text()}]
            leituras = self.m_rel_leituras.ListagemGeral(colunas)
            self.list_leituras.clear()

            for leitura in leituras:
                self.list_leituras.append([leitura['id'], leitura['titulo'], leitura['editora'], leitura['tipo'], leitura['dt'], 'edit-find'])
            
            self.tree_leituras.set_model(self.list_leituras)
        else:
            colunas = [{'coluna':'titulo', 'valor':self.builder.get_object('entry_busca').get_text()}]
            games = self.m_rel_games.ListagemGeral(colunas)
            self.list_games.clear()

            for game in games:
                self.list_games.append([game['id'], game['titulo'], game['console'], game['dt'], 'edit-find'])
            
            self.tree_games.set_model(self.list_games)

    def GeraCsv(self):
        nome_csv = time.strftime("%Y-%m-%d_%H:%M:%S") + '.csv'
        aba = self.builder.get_object("notebook_leituras_games").get_current_page()
        arq = ''
        if aba == 0:
            titulos_csv = ['ID', 'TÍTULO', 'AUTOR', 'EDITORA', 'ASSUNTO', 'TIPO', 'FORMATO', 'ANO', 'NOTA', 'ISBN', 'PAGINAS', 'DT_FINALIZAÇÃO', 'ID_SKOOB']
            m_leituras = LeiturasModel()
            leituras = m_leituras.GetDadosCsv()
            arq = './csvs/leituras/{}'.format(nome_csv)
            with open(arq, 'w', newline='') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
                spamwriter.writerow(titulos_csv)
                for leitura in leituras:
                    spamwriter.writerow([leitura['id'],leitura['titulo'],leitura['autor'],leitura['editora'],leitura['assunto'],leitura['tipo'],leitura['formato'],leitura['ano'],leitura['nota'],leitura['isbn'],leitura['paginas'],leitura['dt'],leitura['id_skoob']])
        else:
            titulos_csv = ['ID','TÍTULO','CONSOLE','PUBLICADORA','DESENVOLVEDORA','GÊNERO','DLC','DIGITAL','DT_FINALIZAÇÃO','ID_IGDB', 'ID_GAMESDB']
            m_games = GamesModel()
            games = m_games.GetDadosCsv()
            arq = './csvs/games/{}'.format(nome_csv)
            with open(arq, 'w', newline='') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
                spamwriter.writerow(titulos_csv)
                for game in games:
                    spamwriter.writerow([game['id'],game['titulo'],game['console'],game['publicadora'],game['desenvolvedora'],game['genero'],game['dlc'],game['digital'],game['dt'],game['id_igdb'],game['id_gamesdb']])

        if sys.platform == 'linux':
            subprocess.check_call(['xdg-open', arq])
        elif sys.platform == 'win32':
            subprocess.check_call(['explorer', arq])

WinPrincipal()