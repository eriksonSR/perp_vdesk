import gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from models.models import *

class WinListaEditoras:
	def __init__(self):
		self.handlers = {
			"busca_editora": self.BuscaEditora
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/editoras/win_lista_editoras.glade'))
		self.builder.connect_signals(self.handlers)
		self.m_editoras = EditorasModel()
		self.list_editoras = Gtk.ListStore(int, str, str)
		self.tree_editoras = self.builder.get_object('treeview_editoras')
		self.ExibeLista()
		self.win_lista_autor = self.builder.get_object("win_lista_editoras")
		self.win_lista_autor.connect("destroy", Gtk.main_quit)
		self.win_lista_autor.show_all()

		Gtk.main()

	def ExibeLista(self):
		editoras = self.m_editoras.Listar()
		for editora in editoras:
			self.list_editoras.append([editora['id'], editora['editora'], 'document-edit'])

		self.tree_editoras.set_model(self.list_editoras)
		
		renderer_id = Gtk.CellRendererText()
		column_id = Gtk.TreeViewColumn("ID", renderer_id, text=0)
		self.tree_editoras.append_column(column_id)
		
		renderer_editora = Gtk.CellRendererText()
		column_autor = Gtk.TreeViewColumn("Editora", renderer_editora, text=1)
		self.tree_editoras.append_column(column_autor)

		renderer_editar = Gtk.CellRendererPixbuf()
		renderer_editar.set_property('xalign', 0.0)
		column_editar = Gtk.TreeViewColumn("Editar", renderer_editar, icon_name=2)
		self.tree_editoras.append_column(column_editar)

		self.tree_editoras.set_activate_on_single_click(True)
		self.tree_editoras.connect("row_activated", self.EditarEditora)

	def EditarEditora(self, treeview, path, column):
		if column.get_title() == 'Editar':
			model, treeiter = treeview.get_selection().get_selected()
			id_editora = model[treeiter][0]
			win_edita_editora = WinEditaEditora(id_editora)

	def BuscaEditora(self, widget):
		entry = self.builder.get_object('entry_editora')
		editora = entry.get_text()
		qtd_chars = len(editora)
		
		if qtd_chars >= 3:
			self.list_editoras.clear()
			editoras = self.m_editoras.BuscaEditoraPorNome(editora)

			for editora in editoras:
				self.list_editoras.append([editora['id'], editora['editora'], 'document-edit'])
			
			self.tree_editoras.set_model(self.list_editoras)
		elif qtd_chars == 0:
			self.list_editoras.clear()
			editoras = self.m_editoras.Listar()

			for editora in editoras:
				self.list_editoras.append([editora['id'], editora['editora'], 'document-edit'])
			
			self.tree_editoras.set_model(self.list_editoras)

class WinEditaEditora:
	def __init__(self, id_editora):
		self.id_editora = id_editora
		self.handlers = {
			"salvar_editora": self.SalvaEditora
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/editoras/win_edita_editora.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_edita_editora = self.builder.get_object("win_edita_editora")
		self.win_edita_editora.connect("destroy", Gtk.main_quit)
		self.win_edita_editora.show_all()
		self.GetEditora()

		Gtk.main()

	def SalvaEditora(self, widget):
		entry = self.builder.get_object('entry_editora')
		editora_nome = entry.get_text()
		editora = {'editora':editora_nome, 'id':self.id_editora}
		m_editora = EditorasModel()
		if not m_editora.TestaSeExisteEditora(editora_nome):
			m_editora.Atualizar(editora)
			self.Info('sucesso', 'Editora atualizada com sucesso!')
		else:
			self.Info('erro', 'Editora já cadastrada!')

	def GetEditora(self):
		m_editora = EditorasModel()
		editora = m_editora.GetEditoraById(self.id_editora)

		entry = self.builder.get_object('entry_editora')
		entry.set_text(editora['editora'])

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()

class WinAdicionaEditora:
	def __init__(self):
		self.handlers = {
			"add_editora": self.AdicionaEditora
		}
		self.builder = Gtk.Builder()
		self.builder.add_from_file(os.path.realpath('./janelas/editoras/win_add_editora.glade'))
		self.builder.connect_signals(self.handlers)
		self.win_add_editora = self.builder.get_object("win_add_editora")
		self.win_add_editora.connect("destroy", Gtk.main_quit)
		self.win_add_editora.show_all()

		Gtk.main()

	def AdicionaEditora(self, widget):
		entry = self.builder.get_object('entry_editora')
		editora = entry.get_text()
		m_editora = EditorasModel()
		if not m_editora.TestaSeExisteEditora(editora):
			m_editora.Inserir(editora)
			entry.set_text('')
			self.Info('sucesso', 'Editora cadastrado com sucesso!')
		else:
			self.Info('erro', 'Editora já cadastrado!')

	def Info(self, tipo, msg):
		titulo = ''
		if tipo == 'erro':
			titulo = 'ERRO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, msg)
		else:
			titulo = 'SUCESSO'
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CANCEL, msg)

		dialog.set_title(titulo)
		dialog.run()
		dialog.destroy()